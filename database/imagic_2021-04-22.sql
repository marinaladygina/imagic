# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.28-0ubuntu0.18.04.4)
# Database: imagic
# Generation Time: 2021-04-22 02:55:22 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table craft_assetindexdata
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assetindexdata`;

CREATE TABLE `craft_assetindexdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionId` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `volumeId` int(11) NOT NULL,
  `uri` text COLLATE utf8_unicode_ci,
  `size` bigint(20) unsigned DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `recordId` int(11) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT '0',
  `completed` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_vedahlphczcclwuxqxqulpxrssdspvmvzwmw` (`sessionId`,`volumeId`),
  KEY `craft_idx_bxsngvrnrexpgfupiqgrztazfphzdgutjebr` (`volumeId`),
  CONSTRAINT `craft_fk_rtxkppcfwzxiasmqkwyqbkopshlngzpsgdyg` FOREIGN KEY (`volumeId`) REFERENCES `craft_volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_assets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assets`;

CREATE TABLE `craft_assets` (
  `id` int(11) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `uploaderId` int(11) DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kind` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `size` bigint(20) unsigned DEFAULT NULL,
  `focalPoint` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deletedWithVolume` tinyint(1) DEFAULT NULL,
  `keptFile` tinyint(1) DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_yvezoidcvnstxjufzlzggrinimmnmftfklde` (`filename`,`folderId`),
  KEY `craft_idx_unkeqtehtlcvqhtjjsclahkgzafkhfmidkme` (`folderId`),
  KEY `craft_idx_zsfonhxklngcaoajnbpyenxathtbqrhclklx` (`volumeId`),
  KEY `craft_fk_ijpnejocczsdbdtlcaupalchkbhvhibbmbdg` (`uploaderId`),
  CONSTRAINT `craft_fk_bmggqupplffvflzrdojltxliqrdztabxmfbf` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_ijpnejocczsdbdtlcaupalchkbhvhibbmbdg` FOREIGN KEY (`uploaderId`) REFERENCES `craft_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_fk_kmwdqwfhmblfjborabpeknthrqyqeltxlzik` FOREIGN KEY (`volumeId`) REFERENCES `craft_volumes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_xwvhnzgkweuhoxfllttnkkykcztwkvswpdvz` FOREIGN KEY (`folderId`) REFERENCES `craft_volumefolders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_assets` WRITE;
/*!40000 ALTER TABLE `craft_assets` DISABLE KEYS */;

INSERT INTO `craft_assets` (`id`, `volumeId`, `folderId`, `uploaderId`, `filename`, `kind`, `width`, `height`, `size`, `focalPoint`, `deletedWithVolume`, `keptFile`, `dateModified`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(5,1,2,1,'photo-1544198365-f5d60b6d8190.jpeg','image',1350,900,176948,NULL,NULL,NULL,'2021-04-22 01:54:07','2021-04-22 01:54:08','2021-04-22 01:54:08','d9e5d0a3-6256-405d-9381-d13befaf938d'),
	(6,1,2,1,'photo-1547093349-65cdba98369a.jpeg','image',1350,900,209363,NULL,NULL,NULL,'2021-04-22 01:54:17','2021-04-22 01:54:17','2021-04-22 01:54:17','e1ef23ea-5d46-435b-93f9-71916d5ce465'),
	(7,1,2,1,'photo-1483728642387-6c3bdd6c93e5.jpeg','image',1355,897,56242,NULL,NULL,NULL,'2021-04-22 01:54:35','2021-04-22 01:54:35','2021-04-22 01:54:35','6d41a916-e439-4766-b399-eac0a5016f52'),
	(23,1,2,1,'photo-1575908524891-b0bdd2b99f90.jpeg','image',1489,837,184404,NULL,NULL,NULL,'2021-04-22 02:39:12','2021-04-22 02:39:12','2021-04-22 02:39:12','74de21de-d04a-4c77-806a-ff7b9f0d098f');

/*!40000 ALTER TABLE `craft_assets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_assettransformindex
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assettransformindex`;

CREATE TABLE `craft_assettransformindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) NOT NULL DEFAULT '0',
  `inProgress` tinyint(1) NOT NULL DEFAULT '0',
  `error` tinyint(1) NOT NULL DEFAULT '0',
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_azyzvqcwnhyyylqplsanhderbiehugawhdyf` (`volumeId`,`assetId`,`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_assettransforms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assettransforms`;

CREATE TABLE `craft_assettransforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mode` enum('stretch','fit','crop') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'center-center',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quality` int(11) DEFAULT NULL,
  `interlace` enum('none','line','plane','partition') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'none',
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_efigdnsnujfnpgjjdmzmkolduqhilprlolzu` (`name`),
  KEY `craft_idx_oushkzvbbmerijdkaayvfjhwstmuavblkneb` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_categories`;

CREATE TABLE `craft_categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `deletedWithGroup` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_vuyvnbojgrasridktwjqriknpnvxghthausi` (`groupId`),
  KEY `craft_fk_ttcnkgjgvcghlgdvzcmlsqydfzumehfkgscc` (`parentId`),
  CONSTRAINT `craft_fk_fchevjvpbkvtwjoecdlvmtdqmtgphgnthjdi` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_gnyharoybgjdmoargbzmbbtigvvrbxzplrmu` FOREIGN KEY (`groupId`) REFERENCES `craft_categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_ttcnkgjgvcghlgdvzcmlsqydfzumehfkgscc` FOREIGN KEY (`parentId`) REFERENCES `craft_categories` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_categorygroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_categorygroups`;

CREATE TABLE `craft_categorygroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_jocapkahrkccstfidktmjxjedfpbwuejgclz` (`name`),
  KEY `craft_idx_yysmuygwlfmhmgxlgfjhokpkadisiypuikly` (`handle`),
  KEY `craft_idx_mwhjjxckxpnckjrvhofkipvizhzrptjvuvhc` (`structureId`),
  KEY `craft_idx_dvettzvxvwhozmslhnjowbvpkdrnbgnrkmam` (`fieldLayoutId`),
  KEY `craft_idx_zcgcauhpvgzcaqabftaarudgqsqtslpdkopi` (`dateDeleted`),
  CONSTRAINT `craft_fk_fwgijvlnoxhorvpxhegzrebxedusznuakjby` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_fk_tmwrefdghmboeulhbcqlrsidgbvjzrykkebg` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_categorygroups_sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_categorygroups_sites`;

CREATE TABLE `craft_categorygroups_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text COLLATE utf8_unicode_ci,
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_xjnfhyuchwhybzexgilhdhdcgyiodywgqybo` (`groupId`,`siteId`),
  KEY `craft_idx_mkwfuzfehejdzjcftisddbcgtxmqglwrptbs` (`siteId`),
  CONSTRAINT `craft_fk_ifwmjwstmjzbtfjnqbfmyhhnjwfgrfyyajrd` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_fk_phcqadwltzhslhinqwdhsrucuiunthqoaaaq` FOREIGN KEY (`groupId`) REFERENCES `craft_categorygroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_changedattributes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_changedattributes`;

CREATE TABLE `craft_changedattributes` (
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `attribute` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `propagated` tinyint(1) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`elementId`,`siteId`,`attribute`),
  KEY `craft_idx_abltnakoprpaigfvcqwrdbrykvcqwyvwhkpp` (`elementId`,`siteId`,`dateUpdated`),
  KEY `craft_fk_ldhwjkhezmzsnulxjojsdkejgwvqimibykyh` (`siteId`),
  KEY `craft_fk_rordfwkltrldtygzbopnmcruuselvfckdnzy` (`userId`),
  CONSTRAINT `craft_fk_bzjlejuiklpajfdcdobtxkfltcnqvilgwejj` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_fk_ldhwjkhezmzsnulxjojsdkejgwvqimibykyh` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_fk_rordfwkltrldtygzbopnmcruuselvfckdnzy` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_changedattributes` WRITE;
/*!40000 ALTER TABLE `craft_changedattributes` DISABLE KEYS */;

INSERT INTO `craft_changedattributes` (`elementId`, `siteId`, `attribute`, `dateUpdated`, `propagated`, `userId`)
VALUES
	(1,1,'firstName','2021-04-22 01:55:43',0,1),
	(1,1,'lastName','2021-04-22 01:55:43',0,1),
	(2,1,'title','2021-04-22 02:39:16',0,1);

/*!40000 ALTER TABLE `craft_changedattributes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_changedfields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_changedfields`;

CREATE TABLE `craft_changedfields` (
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `propagated` tinyint(1) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`elementId`,`siteId`,`fieldId`),
  KEY `craft_idx_bfwawghlpaoqbnfsuizpfykmslununjhixmr` (`elementId`,`siteId`,`dateUpdated`),
  KEY `craft_fk_ezhypunvwcvyuqhbnrojxkwwnnytxgnmnekn` (`siteId`),
  KEY `craft_fk_cenedixdvbsxiktuuylzsnmojobhxjbsblud` (`fieldId`),
  KEY `craft_fk_addflyclumfjqvuylpabllrfimxlfbxqcdrg` (`userId`),
  CONSTRAINT `craft_fk_addflyclumfjqvuylpabllrfimxlfbxqcdrg` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `craft_fk_cenedixdvbsxiktuuylzsnmojobhxjbsblud` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_fk_ezhypunvwcvyuqhbnrojxkwwnnytxgnmnekn` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_fk_thuevsflkdahrjjmizjxotmujczmmbrfeovu` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_changedfields` WRITE;
/*!40000 ALTER TABLE `craft_changedfields` DISABLE KEYS */;

INSERT INTO `craft_changedfields` (`elementId`, `siteId`, `fieldId`, `dateUpdated`, `propagated`, `userId`)
VALUES
	(2,1,1,'2021-04-22 01:54:45',0,1),
	(2,1,4,'2021-04-22 02:39:16',0,1);

/*!40000 ALTER TABLE `craft_changedfields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_content`;

CREATE TABLE `craft_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_uzoxqygmtgoiwiyrtvmahptytwtcfgcydgtc` (`elementId`,`siteId`),
  KEY `craft_idx_sbevvbwdmrsengvbyvcyanhsvqiyinhydkeo` (`siteId`),
  KEY `craft_idx_nszwwnpgdkfbbxlbotmvnzxnzrixqsufmfiv` (`title`),
  CONSTRAINT `craft_fk_ctgchpedaqfkysrevxhdoezaqzncfwqftdhe` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_eeroptotjytrtwdszwejppydjthktfmavsyj` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_content` WRITE;
/*!40000 ALTER TABLE `craft_content` DISABLE KEYS */;

INSERT INTO `craft_content` (`id`, `elementId`, `siteId`, `title`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,1,NULL,'2021-04-22 01:07:56','2021-04-22 01:55:43','5604932f-443b-4c9a-ba62-4c68c3e46d85'),
	(2,2,1,'Heading','2021-04-22 01:22:00','2021-04-22 02:39:16','eef4935c-e697-4fe5-a281-bf79ad307b45'),
	(3,3,1,'Homepage','2021-04-22 01:22:00','2021-04-22 01:22:00','95247370-9f72-45d2-ae61-b066f65209b4'),
	(4,4,1,'Homepage','2021-04-22 01:52:47','2021-04-22 01:52:47','6c07fd4e-1907-4a6f-a84b-77d8fed17e09'),
	(5,5,1,'Photo 1544198365 f5d60b6d8190','2021-04-22 01:54:08','2021-04-22 01:54:08','aadf04a0-6ecd-4e5a-9223-007974467128'),
	(6,6,1,'Photo 1547093349 65cdba98369a','2021-04-22 01:54:17','2021-04-22 01:54:17','d62603dd-9e9f-4d31-952e-f1297f91b762'),
	(7,7,1,'Photo 1483728642387 6c3bdd6c93e5','2021-04-22 01:54:35','2021-04-22 01:54:35','171c187b-db13-4fc1-a4c6-58fd9898ab6b'),
	(8,11,1,'Homepage','2021-04-22 01:54:45','2021-04-22 01:54:45','8cfe0e46-07f7-4271-8c46-2ffebd6e6484'),
	(9,15,1,'Homepage','2021-04-22 02:28:19','2021-04-22 02:28:19','cf8b806a-f055-4394-a7c4-4c41e80d866e'),
	(10,19,1,'Homepage','2021-04-22 02:28:32','2021-04-22 02:28:32','a3bad689-ae20-424a-81b3-c9d51cef080a'),
	(11,23,1,'Photo 1575908524891 b0bdd2b99f90','2021-04-22 02:39:12','2021-04-22 02:39:12','64fa7221-a4db-4f9e-983d-0fbfe202ff81'),
	(12,24,1,'Heading','2021-04-22 02:39:16','2021-04-22 02:39:16','03a56935-247f-4e46-bf11-3b1909525e56');

/*!40000 ALTER TABLE `craft_content` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_craftidtokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_craftidtokens`;

CREATE TABLE `craft_craftidtokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `accessToken` text COLLATE utf8_unicode_ci NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_fk_qapdlldreexpuckjbncznfseakkuygzomjwz` (`userId`),
  CONSTRAINT `craft_fk_qapdlldreexpuckjbncznfseakkuygzomjwz` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_deprecationerrors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_deprecationerrors`;

CREATE TABLE `craft_deprecationerrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fingerprint` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `line` smallint(6) unsigned DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `traces` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_uuvhzwgrzjllbtxjzgbcmlfuszjosbaxyart` (`key`,`fingerprint`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_deprecationerrors` WRITE;
/*!40000 ALTER TABLE `craft_deprecationerrors` DISABLE KEYS */;

INSERT INTO `craft_deprecationerrors` (`id`, `key`, `fingerprint`, `lastOccurrence`, `file`, `line`, `message`, `traces`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'ElementQuery::getIterator()','/home/vagrant/code/imagic/templates/index.twig:15','2021-04-22 02:10:13','/home/vagrant/code/imagic/templates/index.twig',15,'Looping through element queries directly has been deprecated. Use the `all()` function to fetch the query results before looping over them.','[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/elements/db/ElementQuery.php\",\"line\":575,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::getIterator()\\\", \\\"Looping through element queries directly has been deprecated. Us...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\MatrixBlockQuery\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":140,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"getIterator\",\"args\":null},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":182,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"block_body\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/11/1157b60f7ab6ac58c8110c2f1f526efb3b276cc7237130a7904fed7e8adbd575.php\",\"line\":93,\"class\":\"Twig\\\\Template\",\"method\":\"displayBlock\",\"args\":\"\\\"body\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":103,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":390,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/TemplateWrapper.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"Twig\\\\TemplateWrapper\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Environment.php\",\"line\":318,\"class\":\"Twig\\\\TemplateWrapper\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":390,\"class\":\"Twig\\\\Environment\",\"method\":\"render\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":451,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":257,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], \\\"site\\\"\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/controllers/TemplatesController.php\",\"line\":100,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Controller.php\",\"line\":181,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":190,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Module.php\",\"line\":534,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":274,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/web/Application.php\",\"line\":104,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":259,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Application.php\",\"line\":392,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/web/index.php\",\"line\":26,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]','2021-04-22 02:10:13','2021-04-22 02:10:13','424b28f1-c54e-4959-a687-ed9c31a0f58a'),
	(2,'ElementQuery::getIterator()','/home/vagrant/code/imagic/templates/index.twig:20','2021-04-22 02:10:13','/home/vagrant/code/imagic/templates/index.twig',20,'Looping through element queries directly has been deprecated. Use the `all()` function to fetch the query results before looping over them.','[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/elements/db/ElementQuery.php\",\"line\":575,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::getIterator()\\\", \\\"Looping through element queries directly has been deprecated. Us...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\MatrixBlockQuery\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":180,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"getIterator\",\"args\":null},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":182,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"block_body\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/11/1157b60f7ab6ac58c8110c2f1f526efb3b276cc7237130a7904fed7e8adbd575.php\",\"line\":93,\"class\":\"Twig\\\\Template\",\"method\":\"displayBlock\",\"args\":\"\\\"body\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":103,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":390,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/TemplateWrapper.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"Twig\\\\TemplateWrapper\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Environment.php\",\"line\":318,\"class\":\"Twig\\\\TemplateWrapper\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":390,\"class\":\"Twig\\\\Environment\",\"method\":\"render\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":451,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":257,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], \\\"site\\\"\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/controllers/TemplatesController.php\",\"line\":100,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Controller.php\",\"line\":181,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":190,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Module.php\",\"line\":534,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":274,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/web/Application.php\",\"line\":104,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":259,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Application.php\",\"line\":392,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/web/index.php\",\"line\":26,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]','2021-04-22 02:10:13','2021-04-22 02:10:13','69d3b03d-0f3e-42cc-92ab-6d2bb4f0cf56'),
	(17,'ElementQuery::getIterator()','/home/vagrant/code/imagic/templates/index.twig:16','2021-04-22 02:33:41','/home/vagrant/code/imagic/templates/index.twig',16,'Looping through element queries directly has been deprecated. Use the `all()` function to fetch the query results before looping over them.','[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/elements/db/ElementQuery.php\",\"line\":575,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::getIterator()\\\", \\\"Looping through element queries directly has been deprecated. Us...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\MatrixBlockQuery\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":86,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"getIterator\",\"args\":null},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":182,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"block_body\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/11/1157b60f7ab6ac58c8110c2f1f526efb3b276cc7237130a7904fed7e8adbd575.php\",\"line\":93,\"class\":\"Twig\\\\Template\",\"method\":\"displayBlock\",\"args\":\"\\\"body\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":48,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":390,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/TemplateWrapper.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"Twig\\\\TemplateWrapper\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Environment.php\",\"line\":318,\"class\":\"Twig\\\\TemplateWrapper\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":390,\"class\":\"Twig\\\\Environment\",\"method\":\"render\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":451,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":257,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], \\\"site\\\"\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/controllers/TemplatesController.php\",\"line\":100,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Controller.php\",\"line\":181,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":190,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Module.php\",\"line\":534,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":274,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/web/Application.php\",\"line\":104,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":259,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Application.php\",\"line\":392,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/web/index.php\",\"line\":26,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]','2021-04-22 02:33:41','2021-04-22 02:33:41','7fdb52c3-b716-4dae-92f9-257c210bd67d'),
	(18,'ElementQuery::getIterator()','/home/vagrant/code/imagic/templates/index.twig:21','2021-04-22 02:33:41','/home/vagrant/code/imagic/templates/index.twig',21,'Looping through element queries directly has been deprecated. Use the `all()` function to fetch the query results before looping over them.','[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/elements/db/ElementQuery.php\",\"line\":575,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::getIterator()\\\", \\\"Looping through element queries directly has been deprecated. Us...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\MatrixBlockQuery\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":126,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"getIterator\",\"args\":null},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":182,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"block_body\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/11/1157b60f7ab6ac58c8110c2f1f526efb3b276cc7237130a7904fed7e8adbd575.php\",\"line\":93,\"class\":\"Twig\\\\Template\",\"method\":\"displayBlock\",\"args\":\"\\\"body\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":48,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":390,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/TemplateWrapper.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"Twig\\\\TemplateWrapper\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Environment.php\",\"line\":318,\"class\":\"Twig\\\\TemplateWrapper\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":390,\"class\":\"Twig\\\\Environment\",\"method\":\"render\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":451,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":257,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], \\\"site\\\"\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/controllers/TemplatesController.php\",\"line\":100,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Controller.php\",\"line\":181,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":190,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Module.php\",\"line\":534,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":274,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/web/Application.php\",\"line\":104,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":259,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Application.php\",\"line\":392,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/web/index.php\",\"line\":26,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]','2021-04-22 02:33:41','2021-04-22 02:33:41','593fb7f3-6a03-426b-b073-ecd979da55e7'),
	(89,'ElementQuery::getIterator()','/home/vagrant/code/imagic/templates/index.twig:24','2021-04-22 02:37:43','/home/vagrant/code/imagic/templates/index.twig',24,'Looping through element queries directly has been deprecated. Use the `all()` function to fetch the query results before looping over them.','[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/elements/db/ElementQuery.php\",\"line\":575,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::getIterator()\\\", \\\"Looping through element queries directly has been deprecated. Us...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\MatrixBlockQuery\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":94,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"getIterator\",\"args\":null},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":182,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"block_body\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/11/1157b60f7ab6ac58c8110c2f1f526efb3b276cc7237130a7904fed7e8adbd575.php\",\"line\":93,\"class\":\"Twig\\\\Template\",\"method\":\"displayBlock\",\"args\":\"\\\"body\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":48,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":390,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/TemplateWrapper.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"Twig\\\\TemplateWrapper\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Environment.php\",\"line\":318,\"class\":\"Twig\\\\TemplateWrapper\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":390,\"class\":\"Twig\\\\Environment\",\"method\":\"render\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":451,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":257,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], \\\"site\\\"\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/controllers/TemplatesController.php\",\"line\":100,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Controller.php\",\"line\":181,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":190,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Module.php\",\"line\":534,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":274,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/web/Application.php\",\"line\":104,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":259,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Application.php\",\"line\":392,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/web/index.php\",\"line\":26,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]','2021-04-22 02:37:43','2021-04-22 02:37:43','ff670bd6-91d3-4486-b692-7ce454dbdb60'),
	(90,'ElementQuery::getIterator()','/home/vagrant/code/imagic/templates/index.twig:29','2021-04-22 02:37:43','/home/vagrant/code/imagic/templates/index.twig',29,'Looping through element queries directly has been deprecated. Use the `all()` function to fetch the query results before looping over them.','[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/elements/db/ElementQuery.php\",\"line\":575,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::getIterator()\\\", \\\"Looping through element queries directly has been deprecated. Us...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\MatrixBlockQuery\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":134,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"getIterator\",\"args\":null},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":182,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"block_body\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/11/1157b60f7ab6ac58c8110c2f1f526efb3b276cc7237130a7904fed7e8adbd575.php\",\"line\":93,\"class\":\"Twig\\\\Template\",\"method\":\"displayBlock\",\"args\":\"\\\"body\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":48,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":390,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/TemplateWrapper.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"Twig\\\\TemplateWrapper\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Environment.php\",\"line\":318,\"class\":\"Twig\\\\TemplateWrapper\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":390,\"class\":\"Twig\\\\Environment\",\"method\":\"render\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":451,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":257,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], \\\"site\\\"\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/controllers/TemplatesController.php\",\"line\":100,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Controller.php\",\"line\":181,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":190,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Module.php\",\"line\":534,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":274,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/web/Application.php\",\"line\":104,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":259,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Application.php\",\"line\":392,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/web/index.php\",\"line\":26,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]','2021-04-22 02:37:43','2021-04-22 02:37:43','bf462896-42d1-4065-969d-5c31c097933d'),
	(109,'ElementQuery::getIterator()','/home/vagrant/code/imagic/templates/index.twig:44','2021-04-22 02:50:56','/home/vagrant/code/imagic/templates/index.twig',44,'Looping through element queries directly has been deprecated. Use the `all()` function to fetch the query results before looping over them.','[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/elements/db/ElementQuery.php\",\"line\":575,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::getIterator()\\\", \\\"Looping through element queries directly has been deprecated. Us...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\MatrixBlockQuery\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":137,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"getIterator\",\"args\":null},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":182,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"block_body\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/11/1157b60f7ab6ac58c8110c2f1f526efb3b276cc7237130a7904fed7e8adbd575.php\",\"line\":93,\"class\":\"Twig\\\\Template\",\"method\":\"displayBlock\",\"args\":\"\\\"body\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":48,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":390,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/TemplateWrapper.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"Twig\\\\TemplateWrapper\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Environment.php\",\"line\":318,\"class\":\"Twig\\\\TemplateWrapper\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":390,\"class\":\"Twig\\\\Environment\",\"method\":\"render\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":451,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":257,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], \\\"site\\\"\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/controllers/TemplatesController.php\",\"line\":100,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Controller.php\",\"line\":181,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":190,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Module.php\",\"line\":534,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":274,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/web/Application.php\",\"line\":104,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":259,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Application.php\",\"line\":392,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/web/index.php\",\"line\":26,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]','2021-04-22 02:50:57','2021-04-22 02:50:57','cff3dd0c-4cc6-4bfd-bad0-5214a4614f49'),
	(110,'ElementQuery::getIterator()','/home/vagrant/code/imagic/templates/index.twig:49','2021-04-22 02:50:56','/home/vagrant/code/imagic/templates/index.twig',49,'Looping through element queries directly has been deprecated. Use the `all()` function to fetch the query results before looping over them.','[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/elements/db/ElementQuery.php\",\"line\":575,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::getIterator()\\\", \\\"Looping through element queries directly has been deprecated. Us...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\MatrixBlockQuery\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":177,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"getIterator\",\"args\":null},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":182,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"block_body\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/11/1157b60f7ab6ac58c8110c2f1f526efb3b276cc7237130a7904fed7e8adbd575.php\",\"line\":93,\"class\":\"Twig\\\\Template\",\"method\":\"displayBlock\",\"args\":\"\\\"body\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":48,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":390,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/TemplateWrapper.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"Twig\\\\TemplateWrapper\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Environment.php\",\"line\":318,\"class\":\"Twig\\\\TemplateWrapper\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":390,\"class\":\"Twig\\\\Environment\",\"method\":\"render\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":451,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":257,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], \\\"site\\\"\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/controllers/TemplatesController.php\",\"line\":100,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Controller.php\",\"line\":181,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":190,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Module.php\",\"line\":534,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":274,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/web/Application.php\",\"line\":104,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":259,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Application.php\",\"line\":392,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/web/index.php\",\"line\":26,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]','2021-04-22 02:50:57','2021-04-22 02:50:57','630d0415-48c9-4218-acc4-b80c441cbbe8'),
	(145,'ElementQuery::getIterator()','/home/vagrant/code/imagic/templates/index.twig:43','2021-04-22 02:52:35','/home/vagrant/code/imagic/templates/index.twig',43,'Looping through element queries directly has been deprecated. Use the `all()` function to fetch the query results before looping over them.','[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/elements/db/ElementQuery.php\",\"line\":575,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::getIterator()\\\", \\\"Looping through element queries directly has been deprecated. Us...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\MatrixBlockQuery\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":134,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"getIterator\",\"args\":null},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":182,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"block_body\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/11/1157b60f7ab6ac58c8110c2f1f526efb3b276cc7237130a7904fed7e8adbd575.php\",\"line\":93,\"class\":\"Twig\\\\Template\",\"method\":\"displayBlock\",\"args\":\"\\\"body\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":48,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":390,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/TemplateWrapper.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"Twig\\\\TemplateWrapper\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Environment.php\",\"line\":318,\"class\":\"Twig\\\\TemplateWrapper\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":390,\"class\":\"Twig\\\\Environment\",\"method\":\"render\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":451,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":257,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], \\\"site\\\"\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/controllers/TemplatesController.php\",\"line\":100,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Controller.php\",\"line\":181,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":190,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Module.php\",\"line\":534,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":274,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/web/Application.php\",\"line\":104,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":259,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Application.php\",\"line\":392,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/web/index.php\",\"line\":26,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]','2021-04-22 02:52:36','2021-04-22 02:52:36','b81c82d3-52b7-42c3-873c-54bd0c881ade'),
	(146,'ElementQuery::getIterator()','/home/vagrant/code/imagic/templates/index.twig:48','2021-04-22 02:52:35','/home/vagrant/code/imagic/templates/index.twig',48,'Looping through element queries directly has been deprecated. Use the `all()` function to fetch the query results before looping over them.','[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/elements/db/ElementQuery.php\",\"line\":575,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::getIterator()\\\", \\\"Looping through element queries directly has been deprecated. Us...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\MatrixBlockQuery\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":174,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"getIterator\",\"args\":null},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":182,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"block_body\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/11/1157b60f7ab6ac58c8110c2f1f526efb3b276cc7237130a7904fed7e8adbd575.php\",\"line\":93,\"class\":\"Twig\\\\Template\",\"method\":\"displayBlock\",\"args\":\"\\\"body\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"renderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_renderBlocking\\\"], \\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"], \\\"nonRenderBlocking\\\" => [__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8, \\\"block_nonRenderBlocking\\\"]]\"},{\"objectClass\":\"__TwigTemplate_ffc9fcdfed2d1d634903b33d32d353459768fe12170a75a3b4e2279310619fd8\",\"file\":\"/home/vagrant/code/imagic/storage/runtime/compiled_templates/9e/9e64aed57f9e10aa7964bf5834d48ed4d1d8b75a6238defaa33fc84727c3b9f6.php\",\"line\":48,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":405,\"class\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":378,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"body\\\" => [__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab, \\\"block_body\\\"]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Template.php\",\"line\":390,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_c7f542d305c28c77852024de7621d1e9d57f48b705b5f53a070b4f91700f7eab\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/TemplateWrapper.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"Twig\\\\TemplateWrapper\",\"file\":\"/home/vagrant/code/imagic/vendor/twig/twig/src/Environment.php\",\"line\":318,\"class\":\"Twig\\\\TemplateWrapper\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":390,\"class\":\"Twig\\\\Environment\",\"method\":\"render\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/View.php\",\"line\":451,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":257,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], \\\"site\\\"\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/controllers/TemplatesController.php\",\"line\":100,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"index\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Controller.php\",\"line\":181,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Controller.php\",\"line\":190,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Module.php\",\"line\":534,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":274,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/web/Application.php\",\"line\":104,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"index\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/craftcms/cms/src/web/Application.php\",\"line\":259,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/vendor/yiisoft/yii2/base/Application.php\",\"line\":392,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/vagrant/code/imagic/web/index.php\",\"line\":26,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]','2021-04-22 02:52:36','2021-04-22 02:52:36','fb6d3087-8885-4d82-b388-248bf63c355b');

/*!40000 ALTER TABLE `craft_deprecationerrors` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_drafts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_drafts`;

CREATE TABLE `craft_drafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sourceId` int(11) DEFAULT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `trackChanges` tinyint(1) NOT NULL DEFAULT '0',
  `dateLastMerged` datetime DEFAULT NULL,
  `saved` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `craft_idx_lsiiijobmyxbtxewqwwatnnxonagueoyeysb` (`saved`),
  KEY `craft_fk_qzvrfeehepfgtstkbxycynvpnsotjkxcdelg` (`creatorId`),
  KEY `craft_fk_dzxudprppsorxkpqkffjopsmdodxqlzhfwcv` (`sourceId`),
  CONSTRAINT `craft_fk_dzxudprppsorxkpqkffjopsmdodxqlzhfwcv` FOREIGN KEY (`sourceId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_qzvrfeehepfgtstkbxycynvpnsotjkxcdelg` FOREIGN KEY (`creatorId`) REFERENCES `craft_users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_elementindexsettings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_elementindexsettings`;

CREATE TABLE `craft_elementindexsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_timnhblpcmmzpasddkyohrsbaoqawrmuilbg` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_elements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_elements`;

CREATE TABLE `craft_elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `draftId` int(11) DEFAULT NULL,
  `revisionId` int(11) DEFAULT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_yivqzgpvqgjdxcunwkfhgbimnofggostykol` (`dateDeleted`),
  KEY `craft_idx_kkhmucjubepxjkliznwedoajgscobpxskyri` (`fieldLayoutId`),
  KEY `craft_idx_hsufvzrsrywnpueaktaeaphmnwsnfnrjnrgt` (`type`),
  KEY `craft_idx_zwcnnfxyrdavkgwtoqemzhgxfjpqmvwtbapo` (`enabled`),
  KEY `craft_idx_tldijaatqpijoyprhpvwbvxhiaazxobbhwgf` (`archived`,`dateCreated`),
  KEY `craft_idx_oqjyrhwdomaxvhetzrcwxgneazctizjpnxyh` (`archived`,`dateDeleted`,`draftId`,`revisionId`),
  KEY `craft_fk_nnynujjsfweicstbxsohdrkgnpyerqctvxbf` (`draftId`),
  KEY `craft_fk_rffziqszvxcpfxamvhnqaubgjcfiufinlahj` (`revisionId`),
  CONSTRAINT `craft_fk_govosrxaxhqsggsrmsjpdwcosrnyeburbzvw` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_fk_nnynujjsfweicstbxsohdrkgnpyerqctvxbf` FOREIGN KEY (`draftId`) REFERENCES `craft_drafts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_rffziqszvxcpfxamvhnqaubgjcfiufinlahj` FOREIGN KEY (`revisionId`) REFERENCES `craft_revisions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_elements` WRITE;
/*!40000 ALTER TABLE `craft_elements` DISABLE KEYS */;

INSERT INTO `craft_elements` (`id`, `draftId`, `revisionId`, `fieldLayoutId`, `type`, `enabled`, `archived`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`)
VALUES
	(1,NULL,NULL,NULL,'craft\\elements\\User',1,0,'2021-04-22 01:07:56','2021-04-22 01:55:43',NULL,'f5975ff7-789c-42af-8031-aa499bafa321'),
	(2,NULL,NULL,1,'craft\\elements\\Entry',1,0,'2021-04-22 01:22:00','2021-04-22 02:39:16',NULL,'07656ccc-9040-48c8-81e4-aad7a58ade8d'),
	(3,NULL,1,1,'craft\\elements\\Entry',1,0,'2021-04-22 01:22:00','2021-04-22 01:22:00',NULL,'71400b67-7cf3-451c-bb5e-291672dd259e'),
	(4,NULL,2,1,'craft\\elements\\Entry',1,0,'2021-04-22 01:52:47','2021-04-22 01:52:47',NULL,'23ac491e-285f-479f-8b66-4289c0cc6929'),
	(5,NULL,NULL,2,'craft\\elements\\Asset',1,0,'2021-04-22 01:54:08','2021-04-22 01:54:08',NULL,'f4187e6a-7786-4a7a-bcb2-3f6e713e7a9e'),
	(6,NULL,NULL,2,'craft\\elements\\Asset',1,0,'2021-04-22 01:54:17','2021-04-22 01:54:17',NULL,'431cee30-14f2-43e7-bb03-6a16fd655728'),
	(7,NULL,NULL,2,'craft\\elements\\Asset',1,0,'2021-04-22 01:54:35','2021-04-22 01:54:35',NULL,'fafb33fc-ae94-44ae-880e-1723ca76b74b'),
	(8,NULL,NULL,3,'craft\\elements\\MatrixBlock',1,0,'2021-04-22 01:54:45','2021-04-22 01:54:45',NULL,'6b0e14ac-4ff6-43fe-a1bf-806a0a9f7e24'),
	(9,NULL,NULL,3,'craft\\elements\\MatrixBlock',1,0,'2021-04-22 01:54:45','2021-04-22 01:54:45',NULL,'bd12717b-71eb-47e4-a2c8-733c1c8c8fb3'),
	(10,NULL,NULL,3,'craft\\elements\\MatrixBlock',1,0,'2021-04-22 01:54:45','2021-04-22 01:54:45',NULL,'c08068c0-a6ca-4c4c-9e8c-0d3aeb54d858'),
	(11,NULL,3,1,'craft\\elements\\Entry',1,0,'2021-04-22 01:54:45','2021-04-22 01:54:45',NULL,'999bd168-6a48-4692-b814-bb0d25deb793'),
	(12,NULL,NULL,3,'craft\\elements\\MatrixBlock',1,0,'2021-04-22 01:54:45','2021-04-22 01:54:45',NULL,'c39d8bf6-c483-4f16-8263-0838c9a68162'),
	(13,NULL,NULL,3,'craft\\elements\\MatrixBlock',1,0,'2021-04-22 01:54:45','2021-04-22 01:54:45',NULL,'8d742319-beab-4f03-9f9d-0d1d8dcd912b'),
	(14,NULL,NULL,3,'craft\\elements\\MatrixBlock',1,0,'2021-04-22 01:54:45','2021-04-22 01:54:45',NULL,'bcf142b5-cbe2-4beb-90f2-7ff836072550'),
	(15,NULL,4,1,'craft\\elements\\Entry',1,0,'2021-04-22 02:28:19','2021-04-22 02:28:19',NULL,'e8b626d2-643b-42b2-a08b-01cec8d7ba33'),
	(16,NULL,NULL,3,'craft\\elements\\MatrixBlock',1,0,'2021-04-22 02:28:19','2021-04-22 01:54:45',NULL,'b40ea0ad-3234-4710-8820-d67cb8a19e2a'),
	(17,NULL,NULL,3,'craft\\elements\\MatrixBlock',1,0,'2021-04-22 02:28:19','2021-04-22 01:54:45',NULL,'77ca578d-c748-4a59-846a-295b77f372a5'),
	(18,NULL,NULL,3,'craft\\elements\\MatrixBlock',1,0,'2021-04-22 02:28:19','2021-04-22 01:54:45',NULL,'ae74c8bd-908e-4d6e-9d82-f023b8790693'),
	(19,NULL,5,1,'craft\\elements\\Entry',1,0,'2021-04-22 02:28:32','2021-04-22 02:28:32',NULL,'6eb2ca0f-731c-456f-bd84-3045ec5a52ee'),
	(20,NULL,NULL,3,'craft\\elements\\MatrixBlock',1,0,'2021-04-22 02:28:32','2021-04-22 01:54:45',NULL,'63eda40a-480f-47a5-9449-f0fbbf171ada'),
	(21,NULL,NULL,3,'craft\\elements\\MatrixBlock',1,0,'2021-04-22 02:28:32','2021-04-22 01:54:45',NULL,'600f58bb-be22-4132-aa6c-be5fdc27a9d2'),
	(22,NULL,NULL,3,'craft\\elements\\MatrixBlock',1,0,'2021-04-22 02:28:32','2021-04-22 01:54:45',NULL,'30aba1b9-7e8a-4962-8858-099f7bcf8e4b'),
	(23,NULL,NULL,2,'craft\\elements\\Asset',1,0,'2021-04-22 02:39:12','2021-04-22 02:39:12',NULL,'82ad7db1-41a5-4687-a517-2d50bf775dde'),
	(24,NULL,6,1,'craft\\elements\\Entry',1,0,'2021-04-22 02:39:16','2021-04-22 02:39:16',NULL,'22bee58c-8da0-47e5-bd63-13bcbb5c4a00'),
	(25,NULL,NULL,3,'craft\\elements\\MatrixBlock',1,0,'2021-04-22 02:39:16','2021-04-22 01:54:45',NULL,'88c5f3df-4204-448f-9078-516cb4a42f4b'),
	(26,NULL,NULL,3,'craft\\elements\\MatrixBlock',1,0,'2021-04-22 02:39:16','2021-04-22 01:54:45',NULL,'a0008f1c-d81a-4bac-9115-bb10f47513c5'),
	(27,NULL,NULL,3,'craft\\elements\\MatrixBlock',1,0,'2021-04-22 02:39:16','2021-04-22 01:54:45',NULL,'3f771b96-9a55-4272-ad9a-9971bdffab34');

/*!40000 ALTER TABLE `craft_elements` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_elements_sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_elements_sites`;

CREATE TABLE `craft_elements_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_rokhbjbapvipfypkjhnahbdaxzapuswqnstb` (`elementId`,`siteId`),
  KEY `craft_idx_zhuhryytebrintgvztndajsebxekuidiehne` (`siteId`),
  KEY `craft_idx_xmyvvvvafxvgjuoscjyfqvgkugosyrkaerwz` (`slug`,`siteId`),
  KEY `craft_idx_rxddndntqlmcfclhsbyfsnekjfshnwfbsadh` (`enabled`),
  KEY `craft_idx_yfpeojfmepmpxfbobkdebijtjxibreoweulo` (`uri`,`siteId`),
  CONSTRAINT `craft_fk_kdebfzhmidgcycabdtschetwfwsqgslufmhu` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_fk_rpodefkrrwqaltpvdkiezpjuvpquoxossema` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_elements_sites` WRITE;
/*!40000 ALTER TABLE `craft_elements_sites` DISABLE KEYS */;

INSERT INTO `craft_elements_sites` (`id`, `elementId`, `siteId`, `slug`, `uri`, `enabled`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,1,NULL,NULL,1,'2021-04-22 01:07:56','2021-04-22 01:07:56','455153eb-a8dd-46f7-92ca-090b8156ef91'),
	(2,2,1,'homepage','__home__',1,'2021-04-22 01:22:00','2021-04-22 01:22:00','51b47d70-4098-4e8f-b172-b8704e938dd7'),
	(3,3,1,'homepage','__home__',1,'2021-04-22 01:22:00','2021-04-22 01:22:00','26ae5fa3-4d7c-4df9-a82e-17d810014092'),
	(4,4,1,'homepage','__home__',1,'2021-04-22 01:52:47','2021-04-22 01:52:47','8ff7d0a9-344b-46a3-b0de-7ec91cb70124'),
	(5,5,1,NULL,NULL,1,'2021-04-22 01:54:08','2021-04-22 01:54:08','2be2f5e2-4b21-43fe-a82f-0d354dcacc95'),
	(6,6,1,NULL,NULL,1,'2021-04-22 01:54:17','2021-04-22 01:54:17','826f1f83-5d22-45ef-bd98-6ee30b8502f5'),
	(7,7,1,NULL,NULL,1,'2021-04-22 01:54:35','2021-04-22 01:54:35','ad9f9dec-00f8-47d2-b8c5-f2c94d8744e1'),
	(8,8,1,NULL,NULL,1,'2021-04-22 01:54:45','2021-04-22 01:54:45','cd32d5fb-462e-48e6-8da5-9f14355874a7'),
	(9,9,1,NULL,NULL,1,'2021-04-22 01:54:45','2021-04-22 01:54:45','84cd9a76-4d69-44b5-b871-a5476d2e535b'),
	(10,10,1,NULL,NULL,1,'2021-04-22 01:54:45','2021-04-22 01:54:45','218c47a7-f176-4e9f-ad63-d2e2e33af0a7'),
	(11,11,1,'homepage','__home__',1,'2021-04-22 01:54:45','2021-04-22 01:54:45','2279beda-9660-41a4-b10e-fb6935d76802'),
	(12,12,1,NULL,NULL,1,'2021-04-22 01:54:45','2021-04-22 01:54:45','12ed7ec4-ccd6-44ee-9c70-c0798cf0cf11'),
	(13,13,1,NULL,NULL,1,'2021-04-22 01:54:45','2021-04-22 01:54:45','e9579408-2266-41ee-a627-32fe02eeeeed'),
	(14,14,1,NULL,NULL,1,'2021-04-22 01:54:45','2021-04-22 01:54:45','74883e01-939a-40d6-9dbf-044841722313'),
	(15,15,1,'homepage','__home__',1,'2021-04-22 02:28:19','2021-04-22 02:28:19','2795ed44-627b-4657-8781-1f00d0e6b56a'),
	(16,16,1,NULL,NULL,1,'2021-04-22 02:28:19','2021-04-22 02:28:19','615337a4-02af-423d-9b64-cf1d731fb0bd'),
	(17,17,1,NULL,NULL,1,'2021-04-22 02:28:19','2021-04-22 02:28:19','50d36f0c-d724-43c7-b687-f9a73805b025'),
	(18,18,1,NULL,NULL,1,'2021-04-22 02:28:19','2021-04-22 02:28:19','00560c16-216c-402c-8eb9-00304666450f'),
	(19,19,1,'homepage','__home__',1,'2021-04-22 02:28:32','2021-04-22 02:28:32','6c1eda22-16df-43df-a606-40fb5c7bded9'),
	(20,20,1,NULL,NULL,1,'2021-04-22 02:28:32','2021-04-22 02:28:32','a29f1908-2668-4dfc-8889-3628e185cf52'),
	(21,21,1,NULL,NULL,1,'2021-04-22 02:28:32','2021-04-22 02:28:32','8a09dee5-2945-44af-bcce-cc7c676d7fc9'),
	(22,22,1,NULL,NULL,1,'2021-04-22 02:28:32','2021-04-22 02:28:32','e0384395-1370-4f59-a6dc-2859b5143ca5'),
	(23,23,1,NULL,NULL,1,'2021-04-22 02:39:12','2021-04-22 02:39:12','5e23b8ce-b6e0-4bf1-98e7-eb9d5ef5688f'),
	(24,24,1,'homepage','__home__',1,'2021-04-22 02:39:16','2021-04-22 02:39:16','5703659f-c52e-4469-b7f0-9f8dcbdfea44'),
	(25,25,1,NULL,NULL,1,'2021-04-22 02:39:16','2021-04-22 02:39:16','84107302-33c5-45e3-9a3a-76587ac1b9ef'),
	(26,26,1,NULL,NULL,1,'2021-04-22 02:39:16','2021-04-22 02:39:16','11fcb331-bf5c-429f-974a-aebc5785dd96'),
	(27,27,1,NULL,NULL,1,'2021-04-22 02:39:16','2021-04-22 02:39:16','b41e0e37-05b6-424e-9e4a-644fe1c5265d');

/*!40000 ALTER TABLE `craft_elements_sites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_entries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entries`;

CREATE TABLE `craft_entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `typeId` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `deletedWithEntryType` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_krhkczsscycjwdmhgtsxyizlsydbdlhihblb` (`postDate`),
  KEY `craft_idx_eaiiqnhqxzwlfiznrmsoafoqolhnuniciyrw` (`expiryDate`),
  KEY `craft_idx_ezhysbrqeenzzsixudfmqhukbjyzpkbnuvhk` (`authorId`),
  KEY `craft_idx_wnuoqhpnhrpbdyxejkdjvnsprlmskrqswpam` (`sectionId`),
  KEY `craft_idx_jyjeqxchvuciyrfuszclluyeewrnoyeipljv` (`typeId`),
  KEY `craft_fk_eidpmuyfzjvtdcqakrqpjhcyyhxffrxrxeec` (`parentId`),
  CONSTRAINT `craft_fk_eidpmuyfzjvtdcqakrqpjhcyyhxffrxrxeec` FOREIGN KEY (`parentId`) REFERENCES `craft_entries` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_fk_qavkuxespzpkoqacbvrajuvogmoivfaxfppj` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_rhghhlonprpsfimrfxeshtauujedzoyyounx` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_ybxgidrjcgstqivtqsehqfbhkyjgkcinhmdb` FOREIGN KEY (`authorId`) REFERENCES `craft_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_fk_zzhxworjouksckuujipahqdioqqkxhqewnvf` FOREIGN KEY (`typeId`) REFERENCES `craft_entrytypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_entries` WRITE;
/*!40000 ALTER TABLE `craft_entries` DISABLE KEYS */;

INSERT INTO `craft_entries` (`id`, `sectionId`, `parentId`, `typeId`, `authorId`, `postDate`, `expiryDate`, `deletedWithEntryType`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,1,NULL,1,NULL,'2021-04-22 01:22:00',NULL,NULL,'2021-04-22 01:22:00','2021-04-22 01:22:00','e4a7d80a-bea7-4c22-9227-3975b5f0713c'),
	(3,1,NULL,1,NULL,'2021-04-22 01:22:00',NULL,NULL,'2021-04-22 01:22:00','2021-04-22 01:22:00','5f89c1da-e625-4a04-8c43-cc0ff42ccb1e'),
	(4,1,NULL,1,NULL,'2021-04-22 01:22:00',NULL,NULL,'2021-04-22 01:52:47','2021-04-22 01:52:47','3086ae89-b09e-4148-99fc-6e5e6d2ba32f'),
	(11,1,NULL,1,NULL,'2021-04-22 01:22:00',NULL,NULL,'2021-04-22 01:54:45','2021-04-22 01:54:45','5693a0a9-b665-4223-84f3-846d5c29c220'),
	(15,1,NULL,1,NULL,'2021-04-22 01:22:00',NULL,NULL,'2021-04-22 02:28:19','2021-04-22 02:28:19','fd3b1217-0321-4525-9279-e37497c459a3'),
	(19,1,NULL,1,NULL,'2021-04-22 01:22:00',NULL,NULL,'2021-04-22 02:28:32','2021-04-22 02:28:32','f183b0d6-ea02-4ce4-b118-7efdfba2a4f9'),
	(24,1,NULL,1,NULL,'2021-04-22 01:22:00',NULL,NULL,'2021-04-22 02:39:16','2021-04-22 02:39:16','938d7500-8f8b-4050-a7a3-fe1229213b3a');

/*!40000 ALTER TABLE `craft_entries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_entrytypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entrytypes`;

CREATE TABLE `craft_entrytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasTitleField` tinyint(1) NOT NULL DEFAULT '1',
  `titleTranslationMethod` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'site',
  `titleTranslationKeyFormat` text COLLATE utf8_unicode_ci,
  `titleFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_eiasjtmamclqjezqtkpswrpkvzlrcyqybqyo` (`name`,`sectionId`),
  KEY `craft_idx_dcahozhxkhqemttfxipfmpoilzagmbhyincg` (`handle`,`sectionId`),
  KEY `craft_idx_meldejdmblpdsowqyguxpoazjrotucnxihpk` (`sectionId`),
  KEY `craft_idx_qmkgtctktweeeaoakroiiccorwidxmbcojfh` (`fieldLayoutId`),
  KEY `craft_idx_hlvvldaaxjqvmxgajetjanhqwbexdtzmqygj` (`dateDeleted`),
  CONSTRAINT `craft_fk_spwqhcgrrdzznmhootbrawumfqudgqcgfmrb` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_yduonsmfhhwxvbztvihyahlulxcnpzgqgaxs` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_entrytypes` WRITE;
/*!40000 ALTER TABLE `craft_entrytypes` DISABLE KEYS */;

INSERT INTO `craft_entrytypes` (`id`, `sectionId`, `fieldLayoutId`, `name`, `handle`, `hasTitleField`, `titleTranslationMethod`, `titleTranslationKeyFormat`, `titleFormat`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`)
VALUES
	(1,1,1,'Homepage','homepage',1,'site',NULL,'{section.name|raw}',1,'2021-04-22 01:22:00','2021-04-22 02:28:32',NULL,'2389b641-0d8e-4d1d-a7b6-a255d9ece2cd');

/*!40000 ALTER TABLE `craft_entrytypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fieldgroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldgroups`;

CREATE TABLE `craft_fieldgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_vtsbilwpnhofktjjmtmemcvbndkphjundxde` (`name`),
  KEY `craft_idx_bvlkizbediwhxexjlyhyetrykbzdhbodpiwd` (`dateDeleted`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fieldgroups` WRITE;
/*!40000 ALTER TABLE `craft_fieldgroups` DISABLE KEYS */;

INSERT INTO `craft_fieldgroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`)
VALUES
	(1,'Common','2021-04-22 01:07:56','2021-04-22 01:07:56',NULL,'b05f5804-6505-4612-aad0-e5fc8a0d09cb'),
	(2,'Hero','2021-04-22 01:49:31','2021-04-22 01:49:31','2021-04-22 02:27:52','5ab59ca2-8d3b-4bb4-9f1e-07e6247b1580');

/*!40000 ALTER TABLE `craft_fieldgroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fieldlayoutfields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldlayoutfields`;

CREATE TABLE `craft_fieldlayoutfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_axvqgguuhcffjonnjqzpyvoqrjjujcfllqnz` (`layoutId`,`fieldId`),
  KEY `craft_idx_nwlcwncmsovsdavbpsafjpkxqbgugcakgggb` (`sortOrder`),
  KEY `craft_idx_osgyxybkiorwlsigfdfbeyhuxyozxidsuads` (`tabId`),
  KEY `craft_idx_xnvljorpvedtocacdltmebduvyjpvzadshev` (`fieldId`),
  CONSTRAINT `craft_fk_apolumkldfoaflnyvmfdsoggpizpkjgaloak` FOREIGN KEY (`tabId`) REFERENCES `craft_fieldlayouttabs` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_tbdjtltfhlfjmmmxdmvmbvnmuvyayovsxkmg` FOREIGN KEY (`layoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_xstqkuhlnrdsyxcrmahsvpfeehsgzcwdbpto` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fieldlayoutfields` WRITE;
/*!40000 ALTER TABLE `craft_fieldlayoutfields` DISABLE KEYS */;

INSERT INTO `craft_fieldlayoutfields` (`id`, `layoutId`, `tabId`, `fieldId`, `required`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(5,3,6,3,1,0,'2021-04-22 02:26:37','2021-04-22 02:26:37','38ef092e-ea47-479c-a964-bb8ee0564189'),
	(6,3,6,2,1,1,'2021-04-22 02:26:37','2021-04-22 02:26:37','e7127d7b-477d-4fa3-8971-ac509be4940c'),
	(9,1,8,4,0,1,'2021-04-22 02:28:32','2021-04-22 02:28:32','33dfba53-929e-464d-8d06-19e69e4993ad'),
	(10,1,8,1,0,3,'2021-04-22 02:28:32','2021-04-22 02:28:32','155a1de8-ba74-4950-8011-423ab74ec5eb');

/*!40000 ALTER TABLE `craft_fieldlayoutfields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fieldlayouts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldlayouts`;

CREATE TABLE `craft_fieldlayouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_pbspanuwakryhudfireqtghntegodxaafirk` (`dateDeleted`),
  KEY `craft_idx_ugqoihdzkpcmelujzjwuxkxriuhozygnayso` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fieldlayouts` WRITE;
/*!40000 ALTER TABLE `craft_fieldlayouts` DISABLE KEYS */;

INSERT INTO `craft_fieldlayouts` (`id`, `type`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`)
VALUES
	(1,'craft\\elements\\Entry','2021-04-22 01:22:00','2021-04-22 01:22:00',NULL,'02e9fba4-4ac5-4282-ab2e-e38c2905d363'),
	(2,'craft\\elements\\Asset','2021-04-22 01:51:12','2021-04-22 01:51:12',NULL,'f22b67dc-4f97-4db4-8b88-9a042cc99733'),
	(3,'craft\\elements\\MatrixBlock','2021-04-22 01:51:18','2021-04-22 01:51:18',NULL,'39f3eb13-b6d6-435e-8eaa-d495c3a1ff22');

/*!40000 ALTER TABLE `craft_fieldlayouts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fieldlayouttabs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldlayouttabs`;

CREATE TABLE `craft_fieldlayouttabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `elements` text COLLATE utf8_unicode_ci,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_nhqremewgbgeduvlvrodqrtwpywajkznjsza` (`sortOrder`),
  KEY `craft_idx_unjdcknpdurcgeyktcgykyxfomrcuzkqffqu` (`layoutId`),
  CONSTRAINT `craft_fk_xurtqetdcvfscsakbnxsiwcawetvkwgenway` FOREIGN KEY (`layoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fieldlayouttabs` WRITE;
/*!40000 ALTER TABLE `craft_fieldlayouttabs` DISABLE KEYS */;

INSERT INTO `craft_fieldlayouttabs` (`id`, `layoutId`, `name`, `elements`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,2,'Content','[{\"type\":\"craft\\\\fieldlayoutelements\\\\AssetTitleField\",\"autocomplete\":false,\"class\":null,\"size\":null,\"name\":null,\"autocorrect\":true,\"autocapitalize\":true,\"disabled\":false,\"readonly\":false,\"title\":null,\"placeholder\":null,\"step\":null,\"min\":null,\"max\":null,\"requirable\":false,\"id\":null,\"containerAttributes\":[],\"inputContainerAttributes\":[],\"labelAttributes\":[],\"orientation\":null,\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"width\":100}]',1,'2021-04-22 01:51:12','2021-04-22 01:51:12','4e3e913b-6b53-4767-83ea-67028047337e'),
	(6,3,'Content','[{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":true,\"width\":75,\"fieldUid\":\"9a54084c-6e32-43e1-bb71-0153f402bea7\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":true,\"width\":25,\"fieldUid\":\"b13e99d8-cba3-4b69-81bc-0f2475ef51ff\"}]',1,'2021-04-22 02:26:37','2021-04-22 02:26:37','f3f1d905-fa46-4a26-b85b-3e710d32f4d9'),
	(8,1,'Content','[{\"type\":\"craft\\\\fieldlayoutelements\\\\EntryTitleField\",\"autocomplete\":false,\"class\":null,\"size\":null,\"name\":null,\"autocorrect\":true,\"autocapitalize\":true,\"disabled\":false,\"readonly\":false,\"title\":null,\"placeholder\":null,\"step\":null,\"min\":null,\"max\":null,\"requirable\":false,\"id\":null,\"containerAttributes\":[],\"inputContainerAttributes\":[],\"labelAttributes\":[],\"orientation\":null,\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"width\":75},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":25,\"fieldUid\":\"f983df67-3391-43eb-9b66-fca30122395e\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\HorizontalRule\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"2f695e2f-09f2-4d55-8db7-2e1a3e8ddc17\"}]',1,'2021-04-22 02:28:32','2021-04-22 02:28:32','404caa62-71d1-40da-85e3-3874b4ae16fe');

/*!40000 ALTER TABLE `craft_fieldlayouttabs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fields`;

CREATE TABLE `craft_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'global',
  `instructions` text COLLATE utf8_unicode_ci,
  `searchable` tinyint(1) NOT NULL DEFAULT '1',
  `translationMethod` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'none',
  `translationKeyFormat` text COLLATE utf8_unicode_ci,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_ipvmhhabguwfejzfeuhorgjahplcidrbergs` (`handle`,`context`),
  KEY `craft_idx_isadjzicsazkazoebxalkzffmjcaoiymmnxy` (`groupId`),
  KEY `craft_idx_yvobxzqxjyidbrwupksulgukxouifjiffiqo` (`context`),
  CONSTRAINT `craft_fk_rpltoxmwwirypbkgsnsqlhpnyzhnpdiqjtlb` FOREIGN KEY (`groupId`) REFERENCES `craft_fieldgroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fields` WRITE;
/*!40000 ALTER TABLE `craft_fields` DISABLE KEYS */;

INSERT INTO `craft_fields` (`id`, `groupId`, `name`, `handle`, `context`, `instructions`, `searchable`, `translationMethod`, `translationKeyFormat`, `type`, `settings`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'Slider','slider','global','',0,'site',NULL,'craft\\fields\\Matrix','{\"contentTable\":\"{{%matrixcontent_slider}}\",\"maxBlocks\":\"\",\"minBlocks\":\"1\",\"propagationMethod\":\"all\"}','2021-04-22 01:51:18','2021-04-22 02:26:36','2f695e2f-09f2-4d55-8db7-2e1a3e8ddc17'),
	(2,NULL,'Image','image','matrixBlockType:40a2e466-827a-47cc-ab67-19eaac358bb0','',0,'site',NULL,'craft\\fields\\Assets','{\"allowSelfRelations\":false,\"allowUploads\":true,\"allowedKinds\":[\"image\"],\"defaultUploadLocationSource\":\"volume:a4e045b6-04b7-4f07-8562-0903dff4098c\",\"defaultUploadLocationSubpath\":\"hero\",\"limit\":\"1\",\"localizeRelations\":false,\"previewMode\":\"full\",\"restrictFiles\":\"1\",\"selectionLabel\":\"\",\"showSiteMenu\":true,\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"singleUploadLocationSource\":\"volume:a4e045b6-04b7-4f07-8562-0903dff4098c\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":\"*\",\"targetSiteId\":null,\"useSingleFolder\":false,\"validateRelatedElements\":false,\"viewMode\":\"large\"}','2021-04-22 01:51:18','2021-04-22 01:52:33','b13e99d8-cba3-4b69-81bc-0f2475ef51ff'),
	(3,NULL,'Heading','heading','matrixBlockType:40a2e466-827a-47cc-ab67-19eaac358bb0','',0,'none',NULL,'craft\\fields\\PlainText','{\"byteLimit\":null,\"charLimit\":null,\"code\":\"\",\"columnType\":null,\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":null,\"uiMode\":\"normal\"}','2021-04-22 01:52:33','2021-04-22 01:52:33','9a54084c-6e32-43e1-bb71-0153f402bea7'),
	(4,1,'Hero image','heroImage','global','',0,'site',NULL,'craft\\fields\\Assets','{\"allowSelfRelations\":false,\"allowUploads\":true,\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:a4e045b6-04b7-4f07-8562-0903dff4098c\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"previewMode\":\"full\",\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"showSiteMenu\":true,\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"singleUploadLocationSource\":\"volume:a4e045b6-04b7-4f07-8562-0903dff4098c\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":\"*\",\"targetSiteId\":null,\"useSingleFolder\":false,\"validateRelatedElements\":false,\"viewMode\":\"large\"}','2021-04-22 02:27:37','2021-04-22 02:27:48','f983df67-3391-43eb-9b66-fca30122395e');

/*!40000 ALTER TABLE `craft_fields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_globalsets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_globalsets`;

CREATE TABLE `craft_globalsets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_zipcqnnntogdyphxbxebwiagunjgviqwouxq` (`name`),
  KEY `craft_idx_hrnsgjypxiavlkkgevdoxavcsixafnrzwolo` (`handle`),
  KEY `craft_idx_vfbsklrugxahlpyrqzzjoituhwndkqnbwwct` (`fieldLayoutId`),
  CONSTRAINT `craft_fk_bjaabejmypbchzzmaxhaicucdazfbdyoxmls` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_nlgyzkoyinkqxinqnovbjukffrnipmbeufuj` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_gqlschemas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_gqlschemas`;

CREATE TABLE `craft_gqlschemas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `scope` text COLLATE utf8_unicode_ci,
  `isPublic` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_gqltokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_gqltokens`;

CREATE TABLE `craft_gqltokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `accessToken` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `expiryDate` datetime DEFAULT NULL,
  `lastUsed` datetime DEFAULT NULL,
  `schemaId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_nriqeqtrwydbgwgzaoinqgmvvlqcfxuxtevw` (`accessToken`),
  UNIQUE KEY `craft_idx_ahabxfffvykzmdqtwjhixkdrnzhltnajehgk` (`name`),
  KEY `craft_fk_zytwfjnssnmaxwdnwdytsehwxzaoeihenlig` (`schemaId`),
  CONSTRAINT `craft_fk_zytwfjnssnmaxwdnwdytsehwxzaoeihenlig` FOREIGN KEY (`schemaId`) REFERENCES `craft_gqlschemas` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_info`;

CREATE TABLE `craft_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `maintenance` tinyint(1) NOT NULL DEFAULT '0',
  `configVersion` char(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '000000000000',
  `fieldVersion` char(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '000000000000',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_info` WRITE;
/*!40000 ALTER TABLE `craft_info` DISABLE KEYS */;

INSERT INTO `craft_info` (`id`, `version`, `schemaVersion`, `maintenance`, `configVersion`, `fieldVersion`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'3.6.12','3.6.8',0,'jvgalaptqmyf','cxphezecaxnn','2021-04-22 01:07:56','2021-04-22 02:28:32','09910340-1a9f-4ef0-8ed2-ac885f15b52d');

/*!40000 ALTER TABLE `craft_info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_matrixblocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_matrixblocks`;

CREATE TABLE `craft_matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `deletedWithOwner` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_rhlkdkgxndflqujygigknbeyzjoeswpktczz` (`ownerId`),
  KEY `craft_idx_xufwyxfmtkkcmdazgigkfunxzqufgjlyicia` (`fieldId`),
  KEY `craft_idx_oinabbmegfsqhyxvnbbgxgorgxgqxkwlgioi` (`typeId`),
  KEY `craft_idx_epuhjqwyqphferzjirsxltdifukqvqdhbpep` (`sortOrder`),
  CONSTRAINT `craft_fk_geqvwofuotfuaelvbirrloqhmzpotodgbtbx` FOREIGN KEY (`ownerId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_grdqkvkzgyrtgxyfrrdtdozpbuxqhlwdvxnm` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_jzkdlrrkmugxifyphvftnwgfxfacxwrovasi` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_maeukpgcfksfzjibgfwlzhehhvheuufphohw` FOREIGN KEY (`typeId`) REFERENCES `craft_matrixblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_matrixblocks` WRITE;
/*!40000 ALTER TABLE `craft_matrixblocks` DISABLE KEYS */;

INSERT INTO `craft_matrixblocks` (`id`, `ownerId`, `fieldId`, `typeId`, `sortOrder`, `deletedWithOwner`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(8,2,1,1,1,NULL,'2021-04-22 01:54:45','2021-04-22 01:54:45','67e4e263-3fe4-40b8-a849-3977f6f128fa'),
	(9,2,1,1,2,NULL,'2021-04-22 01:54:45','2021-04-22 01:54:45','eef8c5cf-ff22-4df6-9a90-fdfd84e521ce'),
	(10,2,1,1,3,NULL,'2021-04-22 01:54:45','2021-04-22 01:54:45','f33eeea3-c50b-469b-9027-6198675a8da1'),
	(12,11,1,1,1,NULL,'2021-04-22 01:54:45','2021-04-22 01:54:45','408eb374-0108-4731-afc1-d2e52e2357c1'),
	(13,11,1,1,2,NULL,'2021-04-22 01:54:45','2021-04-22 01:54:45','bc4d68cb-ba5f-4b21-af03-9ad19727ed0f'),
	(14,11,1,1,3,NULL,'2021-04-22 01:54:45','2021-04-22 01:54:45','91967feb-e6a9-4495-a390-f67cfd8a51d6'),
	(16,15,1,1,1,NULL,'2021-04-22 02:28:19','2021-04-22 02:28:19','edeaf352-cdc9-45f6-af3e-cfa97fe5ec6a'),
	(17,15,1,1,2,NULL,'2021-04-22 02:28:19','2021-04-22 02:28:19','117903b7-94e2-4cb0-a63a-7d68791e7c35'),
	(18,15,1,1,3,NULL,'2021-04-22 02:28:19','2021-04-22 02:28:19','1bbde5e8-914e-4abd-8a15-e2da80559b28'),
	(20,19,1,1,1,NULL,'2021-04-22 02:28:32','2021-04-22 02:28:32','42071fec-1ded-4562-9067-0e3d12028ce7'),
	(21,19,1,1,2,NULL,'2021-04-22 02:28:32','2021-04-22 02:28:32','ce351125-b9c4-4975-8c32-0e89785fd39b'),
	(22,19,1,1,3,NULL,'2021-04-22 02:28:32','2021-04-22 02:28:32','738a8b6e-bcaf-4fbd-915f-26fede1dcd9c'),
	(25,24,1,1,1,NULL,'2021-04-22 02:39:16','2021-04-22 02:39:16','93896cb8-807e-41f2-8d59-21a869b96093'),
	(26,24,1,1,2,NULL,'2021-04-22 02:39:16','2021-04-22 02:39:16','201df83a-c4d0-4c75-93d9-d0e893a5e8c8'),
	(27,24,1,1,3,NULL,'2021-04-22 02:39:16','2021-04-22 02:39:16','760ddca3-17a2-4e7e-a848-dcffa832fffd');

/*!40000 ALTER TABLE `craft_matrixblocks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_matrixblocktypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_matrixblocktypes`;

CREATE TABLE `craft_matrixblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_cighuppsuywanjzfwqvqkqkkpuyvqukxkihc` (`name`,`fieldId`),
  KEY `craft_idx_amcgnlmzqxmrnhfimiapauxjntzhkxpaffej` (`handle`,`fieldId`),
  KEY `craft_idx_ekkdodrgyifcwwkpbbirhcdacyqolmjjtfvc` (`fieldId`),
  KEY `craft_idx_veqwzrfveuoqdirmhqelkvkcaejlbtumvroo` (`fieldLayoutId`),
  CONSTRAINT `craft_fk_diyecqffbxhqboyugonaeslocryphrdqdehw` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_hkydbphuuzlfbaicrwojmvcfuqenkfelvvtm` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_matrixblocktypes` WRITE;
/*!40000 ALTER TABLE `craft_matrixblocktypes` DISABLE KEYS */;

INSERT INTO `craft_matrixblocktypes` (`id`, `fieldId`, `fieldLayoutId`, `name`, `handle`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,3,'Slide','slide',1,'2021-04-22 01:51:18','2021-04-22 01:51:18','40a2e466-827a-47cc-ab67-19eaac358bb0');

/*!40000 ALTER TABLE `craft_matrixblocktypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_matrixcontent_slider
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_matrixcontent_slider`;

CREATE TABLE `craft_matrixcontent_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `field_slide_heading` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_cstmhmdaymaelrnqdxnllkeamrxttnqvxflv` (`elementId`,`siteId`),
  KEY `craft_idx_rmwrueguzxscucvnxajxyglivaovtuezduez` (`siteId`),
  CONSTRAINT `craft_fk_jfkkzikgbsqwwbqbfgarbhkwgihqzxttkqjq` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_llqoiunwwgudtrotkhomuzefwncwevomlfhx` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_matrixcontent_slider` WRITE;
/*!40000 ALTER TABLE `craft_matrixcontent_slider` DISABLE KEYS */;

INSERT INTO `craft_matrixcontent_slider` (`id`, `elementId`, `siteId`, `dateCreated`, `dateUpdated`, `uid`, `field_slide_heading`)
VALUES
	(1,8,1,'2021-04-22 01:54:45','2021-04-22 01:54:45','6bdf8f3c-0840-4dc2-8fdc-4bba9e3eb712','Title 1'),
	(2,9,1,'2021-04-22 01:54:45','2021-04-22 01:54:45','be30b059-2265-4d82-8a88-533dc995d909','Title 2'),
	(3,10,1,'2021-04-22 01:54:45','2021-04-22 01:54:45','810c198e-bb52-4176-a642-4a820d8dcf91','Title 3'),
	(4,12,1,'2021-04-22 01:54:45','2021-04-22 01:54:45','c5b03a8b-e14a-4bea-b692-5879ec8c0655','Title 1'),
	(5,13,1,'2021-04-22 01:54:45','2021-04-22 01:54:45','26d6d847-a7cc-409a-bdf1-f488d8017e66','Title 2'),
	(6,14,1,'2021-04-22 01:54:45','2021-04-22 01:54:45','478bfbcb-c57a-48fb-be4c-61c8b11ab4d0','Title 3'),
	(7,16,1,'2021-04-22 02:28:19','2021-04-22 02:28:19','39063db6-3b26-4f8b-a468-e10e2b40c1d6','Title 1'),
	(8,17,1,'2021-04-22 02:28:19','2021-04-22 02:28:19','fee3d327-fbe5-4b16-a47c-86b2b8ed5429','Title 2'),
	(9,18,1,'2021-04-22 02:28:19','2021-04-22 02:28:19','4eeb4d6a-9d89-4f45-822d-12fb1a33760b','Title 3'),
	(10,20,1,'2021-04-22 02:28:32','2021-04-22 02:28:32','94279ac4-9163-4872-9b4c-fd0d7e1366dd','Title 1'),
	(11,21,1,'2021-04-22 02:28:32','2021-04-22 02:28:32','77fd7866-e2e5-4c3d-bf7a-802a992e9d04','Title 2'),
	(12,22,1,'2021-04-22 02:28:32','2021-04-22 02:28:32','f5cd9ec5-ee6b-4693-915f-f43ddc00c911','Title 3'),
	(13,25,1,'2021-04-22 02:39:16','2021-04-22 02:39:16','bccaaa1c-d900-4a9d-9a6e-de60707c4db8','Title 1'),
	(14,26,1,'2021-04-22 02:39:16','2021-04-22 02:39:16','a9c422b2-16ba-440a-ae30-9912c0a6b9ab','Title 2'),
	(15,27,1,'2021-04-22 02:39:16','2021-04-22 02:39:16','6e1e7dbc-54f5-4e0c-a98d-4a6ac9e07296','Title 3');

/*!40000 ALTER TABLE `craft_matrixcontent_slider` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_migrations`;

CREATE TABLE `craft_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `track` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_gpsomcikdyzuwsiafxhjtcplwawgcojonqvl` (`track`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_migrations` WRITE;
/*!40000 ALTER TABLE `craft_migrations` DISABLE KEYS */;

INSERT INTO `craft_migrations` (`id`, `track`, `name`, `applyTime`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'craft','Install','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','7429067c-8424-4411-be60-8eb2648dcd39'),
	(2,'craft','m150403_183908_migrations_table_changes','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','9f7da29f-fd0f-4f8b-bdab-e552732e0dee'),
	(3,'craft','m150403_184247_plugins_table_changes','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','93389ced-4384-4476-a810-2165562c909e'),
	(4,'craft','m150403_184533_field_version','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','2dbbdf1a-a7c9-4cc1-8a6e-6c32d0e03465'),
	(5,'craft','m150403_184729_type_columns','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','243ec793-da6b-41a6-bbc6-1b2f15992aa7'),
	(6,'craft','m150403_185142_volumes','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','b131336c-b2e8-4f38-adb3-48d7c884fb55'),
	(7,'craft','m150428_231346_userpreferences','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','c5567f31-69de-446e-88b1-d06958b89168'),
	(8,'craft','m150519_150900_fieldversion_conversion','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','95d3f173-8f07-431e-b967-d45c7f05eabc'),
	(9,'craft','m150617_213829_update_email_settings','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','84ae7506-48c0-4d1a-86d7-2597ac4847ad'),
	(10,'craft','m150721_124739_templatecachequeries','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','3853bc2b-1d6a-4d3a-a6df-279c935e057f'),
	(11,'craft','m150724_140822_adjust_quality_settings','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','27510844-0d4d-43fe-ad72-9b7e7bdbd21c'),
	(12,'craft','m150815_133521_last_login_attempt_ip','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','1e937e56-a212-4921-b3f7-e0075bb450f1'),
	(13,'craft','m151002_095935_volume_cache_settings','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','392b45f4-0745-4d5b-bf37-44a98061df15'),
	(14,'craft','m151005_142750_volume_s3_storage_settings','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','f106454b-9d5c-46ec-91de-61f9934fe5e5'),
	(15,'craft','m151016_133600_delete_asset_thumbnails','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','11eed14e-46b1-42f5-af49-88f4103583ab'),
	(16,'craft','m151209_000000_move_logo','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','c3610fb7-cd29-4fec-b92d-6393dfd39219'),
	(17,'craft','m151211_000000_rename_fileId_to_assetId','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','f0ca3a4d-211d-4d7f-85ef-828b9fcdc407'),
	(18,'craft','m151215_000000_rename_asset_permissions','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','e6eb2e99-68ab-4598-9900-2ecc5964530a'),
	(19,'craft','m160707_000001_rename_richtext_assetsource_setting','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','f01f1864-51c7-46f8-9fcf-d15d2cdd19ed'),
	(20,'craft','m160708_185142_volume_hasUrls_setting','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','493471da-374f-4f09-918a-d5a0387e1e58'),
	(21,'craft','m160714_000000_increase_max_asset_filesize','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','8a0c8255-4461-4de4-90eb-5e3a058a79d6'),
	(22,'craft','m160727_194637_column_cleanup','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','aee17543-a192-4eb2-9c67-3eaec1645142'),
	(23,'craft','m160804_110002_userphotos_to_assets','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','75d6553c-01c1-4e90-8c4b-22db4dda9fbd'),
	(24,'craft','m160807_144858_sites','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','ea5f58f3-db58-4427-bd2f-e13bb5f45f0e'),
	(25,'craft','m160829_000000_pending_user_content_cleanup','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','6e475a0c-50f0-45d3-b6cf-1523bb1adc07'),
	(26,'craft','m160830_000000_asset_index_uri_increase','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','4f30477a-1be3-4a0f-a924-65031d951c2c'),
	(27,'craft','m160912_230520_require_entry_type_id','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','b884442d-b341-4947-a644-1a218fcd71f1'),
	(28,'craft','m160913_134730_require_matrix_block_type_id','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','4f791d3a-930a-45e5-a0cc-5360e0b69c4e'),
	(29,'craft','m160920_174553_matrixblocks_owner_site_id_nullable','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','f0aaf894-257f-4455-aeba-cc66cb1f24bb'),
	(30,'craft','m160920_231045_usergroup_handle_title_unique','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','da2d54d9-89e9-47f9-9af3-f43cc89e2c0b'),
	(31,'craft','m160925_113941_route_uri_parts','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','0d4bd059-a24e-49bb-b9f2-c2b5bd2e17c3'),
	(32,'craft','m161006_205918_schemaVersion_not_null','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','2c5b5517-93d1-42e8-9930-c0fbaddc3543'),
	(33,'craft','m161007_130653_update_email_settings','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','60300e01-e803-4bdd-9b44-b20f6b631aba'),
	(34,'craft','m161013_175052_newParentId','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','f9cbdf4b-e57c-4b85-b96a-ca807359c93f'),
	(35,'craft','m161021_102916_fix_recent_entries_widgets','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','d737daaf-f463-4d42-8b24-d741868de0ca'),
	(36,'craft','m161021_182140_rename_get_help_widget','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','db9cf18c-042a-40ea-a56c-fbb6eccabc81'),
	(37,'craft','m161025_000000_fix_char_columns','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','6d7e3820-c22f-42a9-bb70-ed3ea8b3887c'),
	(38,'craft','m161029_124145_email_message_languages','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','6ce077a8-1ba0-4153-a6d5-ab3f2ca48b74'),
	(39,'craft','m161108_000000_new_version_format','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','bbd0dc65-b9cf-43c4-94ca-7f3d7f0beadb'),
	(40,'craft','m161109_000000_index_shuffle','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','ada72031-3be3-4e2d-ae62-92e8ed0facee'),
	(41,'craft','m161122_185500_no_craft_app','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','7710a0d2-97a2-4772-96ca-4f96d07950e9'),
	(42,'craft','m161125_150752_clear_urlmanager_cache','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','b2eb9a2e-3426-4c70-a1bc-1224a0307d40'),
	(43,'craft','m161220_000000_volumes_hasurl_notnull','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','b902f794-a640-4ba5-89b6-69c290674caf'),
	(44,'craft','m170114_161144_udates_permission','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','a8807d35-f25a-4b5e-917f-f8f4b1b971aa'),
	(45,'craft','m170120_000000_schema_cleanup','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','806c3a1d-7552-401e-a222-93164db1fc16'),
	(46,'craft','m170126_000000_assets_focal_point','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','7ef54b9a-9b5c-4fe9-8a45-79a275acfc44'),
	(47,'craft','m170206_142126_system_name','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','36b4daf3-e6c9-4bd3-89dd-29fc183d8bb9'),
	(48,'craft','m170217_044740_category_branch_limits','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','b6d10a27-a94f-453f-a557-219beb0adb3d'),
	(49,'craft','m170217_120224_asset_indexing_columns','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','18628ff9-f6bd-4780-9e8c-a153b5502ec7'),
	(50,'craft','m170223_224012_plain_text_settings','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','7a3cbb7e-dc73-4e98-8369-2d4f7990cd50'),
	(51,'craft','m170227_120814_focal_point_percentage','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','50bbff50-913e-4fe6-9ca9-0ff8a32340fe'),
	(52,'craft','m170228_171113_system_messages','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','8ab4f857-6f18-4da1-9375-e211b43c1b76'),
	(53,'craft','m170303_140500_asset_field_source_settings','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','65dbce05-4cad-4590-8a25-6d0f08aa2f73'),
	(54,'craft','m170306_150500_asset_temporary_uploads','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','159c8247-8e10-44f7-86f0-407fc339d989'),
	(55,'craft','m170523_190652_element_field_layout_ids','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','8ceeb436-90b2-471b-9b8d-aa8b25914035'),
	(56,'craft','m170621_195237_format_plugin_handles','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','f0cb88ab-469c-4e51-a2bd-2090dcf4088d'),
	(57,'craft','m170630_161027_deprecation_line_nullable','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','4e1e600c-34ae-4f19-a8dc-9732af183347'),
	(58,'craft','m170630_161028_deprecation_changes','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','ff30a9ea-e6cc-4d38-a2d9-a12b3ccd660e'),
	(59,'craft','m170703_181539_plugins_table_tweaks','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','ec4537c3-e1f8-476b-a704-ac0f8d8dd1f3'),
	(60,'craft','m170704_134916_sites_tables','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','30c997b4-2966-4d50-8ed1-345727484d5b'),
	(61,'craft','m170706_183216_rename_sequences','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','89478b24-ca06-4076-9f94-d965e70f4ffa'),
	(62,'craft','m170707_094758_delete_compiled_traits','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','8b243b70-37ba-4e35-9a35-44cb81e11ad6'),
	(63,'craft','m170731_190138_drop_asset_packagist','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','886a27f6-70a0-4f2f-bd0b-a548b1aacd4a'),
	(64,'craft','m170810_201318_create_queue_table','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','13a0b7de-86ed-4144-9b6a-17f210bab4df'),
	(65,'craft','m170903_192801_longblob_for_queue_jobs','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','bc537712-3e28-46e4-843d-cf0d8a9db4c3'),
	(66,'craft','m170914_204621_asset_cache_shuffle','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','20ba478b-46c6-4c29-98d1-e12aaa2371b2'),
	(67,'craft','m171011_214115_site_groups','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','e78d4b5b-51b1-4bd6-a6ff-1106a76253d4'),
	(68,'craft','m171012_151440_primary_site','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','36cb1a82-2b43-4327-b836-69e2df9899d2'),
	(69,'craft','m171013_142500_transform_interlace','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','ab48b6a7-8dfd-43e1-9aaa-defe67b53c07'),
	(70,'craft','m171016_092553_drop_position_select','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','803f73ab-e4e4-49ab-9907-2e3df8e4fa33'),
	(71,'craft','m171016_221244_less_strict_translation_method','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','a33a5acf-35bb-4a38-a09a-b88ba8058520'),
	(72,'craft','m171107_000000_assign_group_permissions','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','a79bc1c8-6cf6-470a-93ec-a60e27bc2a96'),
	(73,'craft','m171117_000001_templatecache_index_tune','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','83067246-27e6-4598-8679-cf1019a400f9'),
	(74,'craft','m171126_105927_disabled_plugins','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','6e5646d9-5199-47d9-b186-29e2793b16e7'),
	(75,'craft','m171130_214407_craftidtokens_table','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','79c588c6-80da-4f3b-99b3-c89baf15f304'),
	(76,'craft','m171202_004225_update_email_settings','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','997d1b49-fa90-4ca5-9668-85f813362991'),
	(77,'craft','m171204_000001_templatecache_index_tune_deux','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','c707f5e1-1ea8-4931-a3c3-eb8d26f74835'),
	(78,'craft','m171205_130908_remove_craftidtokens_refreshtoken_column','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','3f010669-d56c-4cdb-b401-caa9745d9f8a'),
	(79,'craft','m171218_143135_longtext_query_column','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','b1f76767-b473-4460-9067-8819027c8fb8'),
	(80,'craft','m171231_055546_environment_variables_to_aliases','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','11ab0334-3fc8-4aaa-b458-66d328a351d4'),
	(81,'craft','m180113_153740_drop_users_archived_column','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','4aa4c22e-5b37-4549-b955-f50a06443502'),
	(82,'craft','m180122_213433_propagate_entries_setting','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','e83313ac-c729-4a9b-96b6-b905e3d5523d'),
	(83,'craft','m180124_230459_fix_propagate_entries_values','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','1c089338-fe76-430e-96eb-c96920bef213'),
	(84,'craft','m180128_235202_set_tag_slugs','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','32681528-ae0a-4908-88b6-ff6d4c0b87af'),
	(85,'craft','m180202_185551_fix_focal_points','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','facbb10e-13e6-4013-8c1d-9ac41eb46165'),
	(86,'craft','m180217_172123_tiny_ints','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','62dcc98b-bc1a-4f54-b41f-c660eefc36c8'),
	(87,'craft','m180321_233505_small_ints','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','1ee6e87c-9045-43f3-8ea7-6e528977eb11'),
	(88,'craft','m180404_182320_edition_changes','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','89fbce62-d864-458c-ab7d-4accb4f67420'),
	(89,'craft','m180411_102218_fix_db_routes','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','a4ae0974-dbbb-472e-9e55-52c191666c17'),
	(90,'craft','m180416_205628_resourcepaths_table','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','f557420e-2e06-4302-86ad-e70fcf5594ad'),
	(91,'craft','m180418_205713_widget_cleanup','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','95e8d9f8-726e-4a25-ac8e-c0a72e459fb8'),
	(92,'craft','m180425_203349_searchable_fields','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','5101e7c4-894f-48fa-bb06-7464027b9f63'),
	(93,'craft','m180516_153000_uids_in_field_settings','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','c70819c4-9e48-46ae-8cf2-49c72741f405'),
	(94,'craft','m180517_173000_user_photo_volume_to_uid','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','69b0fa90-42ee-426a-bae0-b72016bb039a'),
	(95,'craft','m180518_173000_permissions_to_uid','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','cd42ca97-af5e-4903-996e-295b2a8ef192'),
	(96,'craft','m180520_173000_matrix_context_to_uids','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','4b7a5edb-a1c7-4da4-8076-7d3691f4ed06'),
	(97,'craft','m180521_172900_project_config_table','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','09489dbd-4fec-4609-b1b4-1ffc4604a24d'),
	(98,'craft','m180521_173000_initial_yml_and_snapshot','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','2cd84d5f-a906-4c12-94e1-2ebf30cca466'),
	(99,'craft','m180731_162030_soft_delete_sites','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','6ad88a70-c300-46e3-8d5d-4b1560139013'),
	(100,'craft','m180810_214427_soft_delete_field_layouts','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','7cc7ec60-b693-4058-8116-6f0a4d7594ea'),
	(101,'craft','m180810_214439_soft_delete_elements','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','cb9d5eb8-9f53-47c1-91d5-b012b517427d'),
	(102,'craft','m180824_193422_case_sensitivity_fixes','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','e5fdb281-66ed-41f4-bd73-f457e7a5ea8b'),
	(103,'craft','m180901_151639_fix_matrixcontent_tables','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','9bd8f61b-9cc0-41ca-bcaa-1c23639bf9fc'),
	(104,'craft','m180904_112109_permission_changes','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','dd16d408-e08e-4721-8a1c-f9393a0941b6'),
	(105,'craft','m180910_142030_soft_delete_sitegroups','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','fd6a1543-bbae-48de-bffe-38407770be10'),
	(106,'craft','m181011_160000_soft_delete_asset_support','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','bbf47771-2ab0-4701-abf8-611260952ab2'),
	(107,'craft','m181016_183648_set_default_user_settings','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','6b1ba037-d628-4d84-97bc-c667796a9a19'),
	(108,'craft','m181017_225222_system_config_settings','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','8a48a9f1-c17d-4c33-8e7e-4abb1c27c727'),
	(109,'craft','m181018_222343_drop_userpermissions_from_config','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','1e85bc4e-9843-4b71-a680-f9949e841aae'),
	(110,'craft','m181029_130000_add_transforms_routes_to_config','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','d85ec669-2b97-4bf2-bcb3-50cfd2753d04'),
	(111,'craft','m181112_203955_sequences_table','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','3a763594-6c35-4fbe-9bfc-cf8214fd033e'),
	(112,'craft','m181121_001712_cleanup_field_configs','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','1e35a9c0-7d02-4b93-a4a6-809ccb9f428f'),
	(113,'craft','m181128_193942_fix_project_config','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','53663b7a-d8fc-4aca-ba5c-8c11190f076d'),
	(114,'craft','m181130_143040_fix_schema_version','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','87ec0348-178b-4096-903e-7bae22f3ce8c'),
	(115,'craft','m181211_143040_fix_entry_type_uids','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','c7efbbe6-f248-4979-8723-c08e12546707'),
	(116,'craft','m181217_153000_fix_structure_uids','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','182bcfa7-d78c-42e4-a36a-914aad7051f8'),
	(117,'craft','m190104_152725_store_licensed_plugin_editions','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','e21ab1ad-97b5-4268-9bdd-5ec6dd6e9d08'),
	(118,'craft','m190108_110000_cleanup_project_config','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','ad555e97-77da-435f-b53a-6bf35574914c'),
	(119,'craft','m190108_113000_asset_field_setting_change','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','e7508370-fdaf-4531-82ab-b0ff2e3df726'),
	(120,'craft','m190109_172845_fix_colspan','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','e8eafc36-80fe-43fa-90dd-7063bff55fe4'),
	(121,'craft','m190110_150000_prune_nonexisting_sites','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','6b01317d-b928-4f36-8a1d-4ff06229690b'),
	(122,'craft','m190110_214819_soft_delete_volumes','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','9517795d-51dd-4735-ab1a-5f734a9f1ddb'),
	(123,'craft','m190112_124737_fix_user_settings','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','c4324eb8-d55f-4474-9a67-3d95851a256f'),
	(124,'craft','m190112_131225_fix_field_layouts','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','c1b73eba-48a6-490e-adb2-99c46189f264'),
	(125,'craft','m190112_201010_more_soft_deletes','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','49fc224f-f20f-4aea-a464-f1b85995f8a4'),
	(126,'craft','m190114_143000_more_asset_field_setting_changes','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','298d221b-9320-4805-b26d-4aa717979d09'),
	(127,'craft','m190121_120000_rich_text_config_setting','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','db8362ca-5495-471f-87c5-c179501f533e'),
	(128,'craft','m190125_191628_fix_email_transport_password','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','874b77ee-8a5f-43ba-9192-8ec9ab1e9229'),
	(129,'craft','m190128_181422_cleanup_volume_folders','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','aadcd362-6003-4cc1-ab77-7da5dce33e45'),
	(130,'craft','m190205_140000_fix_asset_soft_delete_index','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','50427427-1e38-447d-9377-6399cf4ea62f'),
	(131,'craft','m190218_143000_element_index_settings_uid','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','b55257c4-bdc3-4d7f-a1a2-c6af1726aa3f'),
	(132,'craft','m190312_152740_element_revisions','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','3f824cdc-8953-4953-a744-8e34de2cd89f'),
	(133,'craft','m190327_235137_propagation_method','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','b94d0bd9-d6ac-4952-bed0-4c792f07c456'),
	(134,'craft','m190401_223843_drop_old_indexes','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','18cc6d24-5c6b-4877-bb55-11b2909a5fa0'),
	(135,'craft','m190416_014525_drop_unique_global_indexes','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','5e4253f9-ef39-40df-925f-f0663aee6097'),
	(136,'craft','m190417_085010_add_image_editor_permissions','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','6618bdf6-e9bb-4a45-9d22-511e93e9c459'),
	(137,'craft','m190502_122019_store_default_user_group_uid','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','cd6dfcaf-7d78-487a-9454-a8fb29f5890a'),
	(138,'craft','m190504_150349_preview_targets','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','2abb2c54-a16e-4cf3-ae0c-52e01f377f34'),
	(139,'craft','m190516_184711_job_progress_label','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','2a876bd1-d334-44cd-b3b3-4185d8f592c6'),
	(140,'craft','m190523_190303_optional_revision_creators','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','a6918386-16a1-4032-a546-a9f4e4a750bb'),
	(141,'craft','m190529_204501_fix_duplicate_uids','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','c33600c4-e4aa-4362-a10d-7c509cb1f182'),
	(142,'craft','m190605_223807_unsaved_drafts','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','36410f0b-e019-43d1-b42a-a51375393a80'),
	(143,'craft','m190607_230042_entry_revision_error_tables','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','da5c068b-6545-4257-a4f5-fb18b7e405d2'),
	(144,'craft','m190608_033429_drop_elements_uid_idx','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','a0e38420-e8d0-45d3-9a58-de52348e528a'),
	(145,'craft','m190617_164400_add_gqlschemas_table','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','fca1293a-ddee-4ef1-bfb5-c73c198e0176'),
	(146,'craft','m190624_234204_matrix_propagation_method','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','ea284f2c-c340-476c-97f5-99c4119aea7d'),
	(147,'craft','m190711_153020_drop_snapshots','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','503b1b81-e4c0-4664-85e7-a3feb73edbae'),
	(148,'craft','m190712_195914_no_draft_revisions','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','8628f106-024e-4183-afc2-3e3d3a268ba1'),
	(149,'craft','m190723_140314_fix_preview_targets_column','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','ef770e31-a6ef-48a2-a3cc-f810736dbf4f'),
	(150,'craft','m190820_003519_flush_compiled_templates','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','4a33fe02-2184-4fd8-96c5-177c105d53d2'),
	(151,'craft','m190823_020339_optional_draft_creators','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','7f129d81-c7fb-4675-abd5-fc7dffb8fe7b'),
	(152,'craft','m190913_152146_update_preview_targets','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','52719147-e753-4409-935f-dd5a9e883391'),
	(153,'craft','m191107_122000_add_gql_project_config_support','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','d0810b27-ea33-4763-b162-8105d959da45'),
	(154,'craft','m191204_085100_pack_savable_component_settings','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','87505c95-5ba7-4026-ba5f-3c7aff7060b0'),
	(155,'craft','m191206_001148_change_tracking','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','e7e80dac-b9af-43c1-b41f-38eb818cf6fd'),
	(156,'craft','m191216_191635_asset_upload_tracking','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','35ef2aa5-2620-44e5-9e64-882040a467a9'),
	(157,'craft','m191222_002848_peer_asset_permissions','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','b706f1d4-b3e6-4a7f-b39e-d787473adf5a'),
	(158,'craft','m200127_172522_queue_channels','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','c63735a9-4b60-40b7-bc3b-f1058482e373'),
	(159,'craft','m200211_175048_truncate_element_query_cache','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','6f46b2f7-6588-4cfa-ac6f-8c9d4d3c5ab0'),
	(160,'craft','m200213_172522_new_elements_index','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','2faf1e11-14f8-4ad9-bd67-c3295691d4a6'),
	(161,'craft','m200228_195211_long_deprecation_messages','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','c4d2b005-61ea-4daa-94af-50ff1c2823e5'),
	(162,'craft','m200306_054652_disabled_sites','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','95e809e2-9275-44bf-9965-542670ea847d'),
	(163,'craft','m200522_191453_clear_template_caches','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','0add3dbf-a8d4-44b9-a136-1b965fd4404d'),
	(164,'craft','m200606_231117_migration_tracks','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','205dd9ed-c45b-44a3-8fb2-e0cd5b8056d5'),
	(165,'craft','m200619_215137_title_translation_method','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','ee4cd280-70fb-4424-8c49-075cd1457604'),
	(166,'craft','m200620_005028_user_group_descriptions','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','ae60a65b-59a6-44e5-a5dd-b6cb5f665cda'),
	(167,'craft','m200620_230205_field_layout_changes','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','4ea7823c-f233-4cd3-b097-a99322c36a9b'),
	(168,'craft','m200625_131100_move_entrytypes_to_top_project_config','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','d0eb3ed0-26fd-4fcc-97b6-7400c6a2ef90'),
	(169,'craft','m200629_112700_remove_project_config_legacy_files','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','b363e647-c83f-4748-b273-590ce59129e5'),
	(170,'craft','m200630_183000_drop_configmap','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','057b09a6-d34a-453b-8c57-c004cd1b95fc'),
	(171,'craft','m200715_113400_transform_index_error_flag','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','69650242-2eed-4d55-bde4-23ea93cbc128'),
	(172,'craft','m200716_110900_replace_file_asset_permissions','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','2440ce70-723a-4feb-9626-0163cd4d4981'),
	(173,'craft','m200716_153800_public_token_settings_in_project_config','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','a32c347e-a653-4674-8a2b-6f517f1c5b96'),
	(174,'craft','m200720_175543_drop_unique_constraints','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','a74f0fd3-cb2e-48bd-91aa-9cbeeb26f390'),
	(175,'craft','m200825_051217_project_config_version','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','08572a91-8eb4-41dd-a134-6e649f015b61'),
	(176,'craft','m201116_190500_asset_title_translation_method','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','4913bebf-9b87-40d2-b7da-f866023126cd'),
	(177,'craft','m201124_003555_plugin_trials','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','0eca0e06-2a6b-48f9-8295-127b275523dc'),
	(178,'craft','m210209_135503_soft_delete_field_groups','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','709a4167-0713-4d0b-91f2-4311e2710ae3'),
	(179,'craft','m210212_223539_delete_invalid_drafts','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','487988a6-0ac2-4191-8b6e-84dfb5cb9fb2'),
	(180,'craft','m210214_202731_track_saved_drafts','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','aa21ff60-17a1-4e55-9a28-fbf3bfffda55'),
	(181,'craft','m210223_150900_add_new_element_gql_schema_components','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','ec175d1d-85aa-43c1-8e71-1d6e6a420806'),
	(182,'craft','m210224_162000_add_projectconfignames_table','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','a63006dd-cdaf-41e9-a579-d2f82158e345'),
	(183,'craft','m210326_132000_invalidate_projectconfig_cache','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','edf96ddc-4ecb-4b02-b8f3-6b94fe39305e'),
	(184,'craft','m210331_220322_null_author','2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:07:57','9958c71d-0f60-4256-b81a-92d2899d81af');

/*!40000 ALTER TABLE `craft_migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_plugins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_plugins`;

CREATE TABLE `craft_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `schemaVersion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `licenseKeyStatus` enum('valid','trial','invalid','mismatched','astray','unknown') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `licensedEdition` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_thpvdehzjuixardgfwkqgaucrhopdougmsop` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_plugins` WRITE;
/*!40000 ALTER TABLE `craft_plugins` DISABLE KEYS */;

INSERT INTO `craft_plugins` (`id`, `handle`, `version`, `schemaVersion`, `licenseKeyStatus`, `licensedEdition`, `installDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'mix','1.5.2','1.0.0','unknown',NULL,'2021-04-22 01:37:12','2021-04-22 01:37:12','2021-04-22 02:04:42','bd577cbb-0500-4aa3-8732-87e77418288d'),
	(2,'imager','v2.4.0','2.0.0','unknown',NULL,'2021-04-22 02:04:38','2021-04-22 02:04:38','2021-04-22 02:04:42','010e568a-cd3e-4524-9c89-38c01e6620ac');

/*!40000 ALTER TABLE `craft_plugins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_projectconfig
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_projectconfig`;

CREATE TABLE `craft_projectconfig` (
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_projectconfig` WRITE;
/*!40000 ALTER TABLE `craft_projectconfig` DISABLE KEYS */;

INSERT INTO `craft_projectconfig` (`path`, `value`)
VALUES
	('dateModified','1619058512'),
	('email.fromEmail','\"ladyginamarina@gmail.com\"'),
	('email.fromName','\"Imagic\"'),
	('email.transportType','\"craft\\\\mail\\\\transportadapters\\\\Sendmail\"'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.autocapitalize','true'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.autocomplete','false'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.autocorrect','true'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.class','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.disabled','false'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.id','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.instructions','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.label','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.max','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.min','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.name','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.orientation','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.placeholder','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.readonly','false'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.requirable','false'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.size','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.step','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.tip','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.title','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.type','\"craft\\\\fieldlayoutelements\\\\EntryTitleField\"'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.warning','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.0.width','75'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.1.fieldUid','\"f983df67-3391-43eb-9b66-fca30122395e\"'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.1.instructions','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.1.label','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.1.required','false'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.1.tip','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.1.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.1.warning','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.1.width','25'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.2.type','\"craft\\\\fieldlayoutelements\\\\HorizontalRule\"'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.3.fieldUid','\"2f695e2f-09f2-4d55-8db7-2e1a3e8ddc17\"'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.3.instructions','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.3.label','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.3.required','false'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.3.tip','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.3.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.3.warning','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.elements.3.width','100'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.name','\"Content\"'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.fieldLayouts.02e9fba4-4ac5-4282-ab2e-e38c2905d363.tabs.0.sortOrder','1'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.handle','\"homepage\"'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.hasTitleField','true'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.name','\"Homepage\"'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.section','\"90138bb3-2808-45dd-90b8-553ca7c4d579\"'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.sortOrder','1'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.titleFormat','\"{section.name|raw}\"'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.titleTranslationKeyFormat','null'),
	('entryTypes.2389b641-0d8e-4d1d-a7b6-a255d9ece2cd.titleTranslationMethod','\"site\"'),
	('fieldGroups.b05f5804-6505-4612-aad0-e5fc8a0d09cb.name','\"Common\"'),
	('fields.2f695e2f-09f2-4d55-8db7-2e1a3e8ddc17.contentColumnType','\"string\"'),
	('fields.2f695e2f-09f2-4d55-8db7-2e1a3e8ddc17.fieldGroup','\"b05f5804-6505-4612-aad0-e5fc8a0d09cb\"'),
	('fields.2f695e2f-09f2-4d55-8db7-2e1a3e8ddc17.handle','\"slider\"'),
	('fields.2f695e2f-09f2-4d55-8db7-2e1a3e8ddc17.instructions','\"\"'),
	('fields.2f695e2f-09f2-4d55-8db7-2e1a3e8ddc17.name','\"Slider\"'),
	('fields.2f695e2f-09f2-4d55-8db7-2e1a3e8ddc17.searchable','false'),
	('fields.2f695e2f-09f2-4d55-8db7-2e1a3e8ddc17.settings.contentTable','\"{{%matrixcontent_slider}}\"'),
	('fields.2f695e2f-09f2-4d55-8db7-2e1a3e8ddc17.settings.maxBlocks','\"\"'),
	('fields.2f695e2f-09f2-4d55-8db7-2e1a3e8ddc17.settings.minBlocks','\"1\"'),
	('fields.2f695e2f-09f2-4d55-8db7-2e1a3e8ddc17.settings.propagationMethod','\"all\"'),
	('fields.2f695e2f-09f2-4d55-8db7-2e1a3e8ddc17.translationKeyFormat','null'),
	('fields.2f695e2f-09f2-4d55-8db7-2e1a3e8ddc17.translationMethod','\"site\"'),
	('fields.2f695e2f-09f2-4d55-8db7-2e1a3e8ddc17.type','\"craft\\\\fields\\\\Matrix\"'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.contentColumnType','\"string\"'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.fieldGroup','\"b05f5804-6505-4612-aad0-e5fc8a0d09cb\"'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.handle','\"heroImage\"'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.instructions','\"\"'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.name','\"Hero image\"'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.searchable','false'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.allowedKinds','null'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.allowSelfRelations','false'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.allowUploads','true'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.defaultUploadLocationSource','\"volume:a4e045b6-04b7-4f07-8562-0903dff4098c\"'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.defaultUploadLocationSubpath','\"\"'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.limit','\"1\"'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.localizeRelations','false'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.previewMode','\"full\"'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.restrictFiles','\"\"'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.selectionLabel','\"\"'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.showSiteMenu','true'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.showUnpermittedFiles','false'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.showUnpermittedVolumes','false'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.singleUploadLocationSource','\"volume:a4e045b6-04b7-4f07-8562-0903dff4098c\"'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.singleUploadLocationSubpath','\"\"'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.source','null'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.sources','\"*\"'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.targetSiteId','null'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.useSingleFolder','false'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.validateRelatedElements','false'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.settings.viewMode','\"large\"'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.translationKeyFormat','null'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.translationMethod','\"site\"'),
	('fields.f983df67-3391-43eb-9b66-fca30122395e.type','\"craft\\\\fields\\\\Assets\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.field','\"2f695e2f-09f2-4d55-8db7-2e1a3e8ddc17\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fieldLayouts.39f3eb13-b6d6-435e-8eaa-d495c3a1ff22.tabs.0.elements.0.fieldUid','\"9a54084c-6e32-43e1-bb71-0153f402bea7\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fieldLayouts.39f3eb13-b6d6-435e-8eaa-d495c3a1ff22.tabs.0.elements.0.instructions','null'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fieldLayouts.39f3eb13-b6d6-435e-8eaa-d495c3a1ff22.tabs.0.elements.0.label','null'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fieldLayouts.39f3eb13-b6d6-435e-8eaa-d495c3a1ff22.tabs.0.elements.0.required','true'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fieldLayouts.39f3eb13-b6d6-435e-8eaa-d495c3a1ff22.tabs.0.elements.0.tip','null'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fieldLayouts.39f3eb13-b6d6-435e-8eaa-d495c3a1ff22.tabs.0.elements.0.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fieldLayouts.39f3eb13-b6d6-435e-8eaa-d495c3a1ff22.tabs.0.elements.0.warning','null'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fieldLayouts.39f3eb13-b6d6-435e-8eaa-d495c3a1ff22.tabs.0.elements.0.width','75'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fieldLayouts.39f3eb13-b6d6-435e-8eaa-d495c3a1ff22.tabs.0.elements.1.fieldUid','\"b13e99d8-cba3-4b69-81bc-0f2475ef51ff\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fieldLayouts.39f3eb13-b6d6-435e-8eaa-d495c3a1ff22.tabs.0.elements.1.instructions','null'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fieldLayouts.39f3eb13-b6d6-435e-8eaa-d495c3a1ff22.tabs.0.elements.1.label','null'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fieldLayouts.39f3eb13-b6d6-435e-8eaa-d495c3a1ff22.tabs.0.elements.1.required','true'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fieldLayouts.39f3eb13-b6d6-435e-8eaa-d495c3a1ff22.tabs.0.elements.1.tip','null'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fieldLayouts.39f3eb13-b6d6-435e-8eaa-d495c3a1ff22.tabs.0.elements.1.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fieldLayouts.39f3eb13-b6d6-435e-8eaa-d495c3a1ff22.tabs.0.elements.1.warning','null'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fieldLayouts.39f3eb13-b6d6-435e-8eaa-d495c3a1ff22.tabs.0.elements.1.width','25'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fieldLayouts.39f3eb13-b6d6-435e-8eaa-d495c3a1ff22.tabs.0.name','\"Content\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fieldLayouts.39f3eb13-b6d6-435e-8eaa-d495c3a1ff22.tabs.0.sortOrder','1'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.9a54084c-6e32-43e1-bb71-0153f402bea7.contentColumnType','\"text\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.9a54084c-6e32-43e1-bb71-0153f402bea7.fieldGroup','null'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.9a54084c-6e32-43e1-bb71-0153f402bea7.handle','\"heading\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.9a54084c-6e32-43e1-bb71-0153f402bea7.instructions','\"\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.9a54084c-6e32-43e1-bb71-0153f402bea7.name','\"Heading\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.9a54084c-6e32-43e1-bb71-0153f402bea7.searchable','false'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.9a54084c-6e32-43e1-bb71-0153f402bea7.settings.byteLimit','null'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.9a54084c-6e32-43e1-bb71-0153f402bea7.settings.charLimit','null'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.9a54084c-6e32-43e1-bb71-0153f402bea7.settings.code','\"\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.9a54084c-6e32-43e1-bb71-0153f402bea7.settings.columnType','null'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.9a54084c-6e32-43e1-bb71-0153f402bea7.settings.initialRows','\"4\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.9a54084c-6e32-43e1-bb71-0153f402bea7.settings.multiline','\"\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.9a54084c-6e32-43e1-bb71-0153f402bea7.settings.placeholder','null'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.9a54084c-6e32-43e1-bb71-0153f402bea7.settings.uiMode','\"normal\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.9a54084c-6e32-43e1-bb71-0153f402bea7.translationKeyFormat','null'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.9a54084c-6e32-43e1-bb71-0153f402bea7.translationMethod','\"none\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.9a54084c-6e32-43e1-bb71-0153f402bea7.type','\"craft\\\\fields\\\\PlainText\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.contentColumnType','\"string\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.fieldGroup','null'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.handle','\"image\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.instructions','\"\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.name','\"Image\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.searchable','false'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.allowedKinds.0','\"image\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.allowSelfRelations','false'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.allowUploads','true'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.defaultUploadLocationSource','\"volume:a4e045b6-04b7-4f07-8562-0903dff4098c\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.defaultUploadLocationSubpath','\"hero\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.limit','\"1\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.localizeRelations','false'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.previewMode','\"full\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.restrictFiles','\"1\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.selectionLabel','\"\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.showSiteMenu','true'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.showUnpermittedFiles','false'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.showUnpermittedVolumes','false'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.singleUploadLocationSource','\"volume:a4e045b6-04b7-4f07-8562-0903dff4098c\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.singleUploadLocationSubpath','\"\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.source','null'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.sources','\"*\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.targetSiteId','null'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.useSingleFolder','false'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.validateRelatedElements','false'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.settings.viewMode','\"large\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.translationKeyFormat','null'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.translationMethod','\"site\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.fields.b13e99d8-cba3-4b69-81bc-0f2475ef51ff.type','\"craft\\\\fields\\\\Assets\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.handle','\"slide\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.name','\"Slide\"'),
	('matrixBlockTypes.40a2e466-827a-47cc-ab67-19eaac358bb0.sortOrder','1'),
	('plugins.imager.edition','\"standard\"'),
	('plugins.imager.enabled','true'),
	('plugins.imager.schemaVersion','\"2.0.0\"'),
	('plugins.mix.edition','\"standard\"'),
	('plugins.mix.enabled','true'),
	('plugins.mix.schemaVersion','\"1.0.0\"'),
	('sections.90138bb3-2808-45dd-90b8-553ca7c4d579.enableVersioning','true'),
	('sections.90138bb3-2808-45dd-90b8-553ca7c4d579.handle','\"homepage\"'),
	('sections.90138bb3-2808-45dd-90b8-553ca7c4d579.name','\"Homepage\"'),
	('sections.90138bb3-2808-45dd-90b8-553ca7c4d579.propagationMethod','\"all\"'),
	('sections.90138bb3-2808-45dd-90b8-553ca7c4d579.siteSettings.fa32c586-4ce5-4e4c-9c24-8a86208c284a.enabledByDefault','true'),
	('sections.90138bb3-2808-45dd-90b8-553ca7c4d579.siteSettings.fa32c586-4ce5-4e4c-9c24-8a86208c284a.hasUrls','true'),
	('sections.90138bb3-2808-45dd-90b8-553ca7c4d579.siteSettings.fa32c586-4ce5-4e4c-9c24-8a86208c284a.template','\"index\"'),
	('sections.90138bb3-2808-45dd-90b8-553ca7c4d579.siteSettings.fa32c586-4ce5-4e4c-9c24-8a86208c284a.uriFormat','\"__home__\"'),
	('sections.90138bb3-2808-45dd-90b8-553ca7c4d579.type','\"single\"'),
	('siteGroups.b95b3ab1-40f6-44cc-ac72-0a4ceccd2e87.name','\"Imagic\"'),
	('sites.fa32c586-4ce5-4e4c-9c24-8a86208c284a.baseUrl','\"$PRIMARY_SITE_URL\"'),
	('sites.fa32c586-4ce5-4e4c-9c24-8a86208c284a.handle','\"default\"'),
	('sites.fa32c586-4ce5-4e4c-9c24-8a86208c284a.hasUrls','true'),
	('sites.fa32c586-4ce5-4e4c-9c24-8a86208c284a.language','\"en-US\"'),
	('sites.fa32c586-4ce5-4e4c-9c24-8a86208c284a.name','\"Imagic\"'),
	('sites.fa32c586-4ce5-4e4c-9c24-8a86208c284a.primary','true'),
	('sites.fa32c586-4ce5-4e4c-9c24-8a86208c284a.siteGroup','\"b95b3ab1-40f6-44cc-ac72-0a4ceccd2e87\"'),
	('sites.fa32c586-4ce5-4e4c-9c24-8a86208c284a.sortOrder','1'),
	('system.edition','\"solo\"'),
	('system.live','true'),
	('system.name','\"Imagic\"'),
	('system.schemaVersion','\"3.6.8\"'),
	('system.timeZone','\"America/Los_Angeles\"'),
	('users.allowPublicRegistration','false'),
	('users.defaultGroup','null'),
	('users.photoSubpath','null'),
	('users.photoVolumeUid','null'),
	('users.requireEmailVerification','true'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.autocapitalize','true'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.autocomplete','false'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.autocorrect','true'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.class','null'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.disabled','false'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.id','null'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.instructions','null'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.label','null'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.max','null'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.min','null'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.name','null'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.orientation','null'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.placeholder','null'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.readonly','false'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.requirable','false'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.size','null'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.step','null'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.tip','null'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.title','null'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.type','\"craft\\\\fieldlayoutelements\\\\AssetTitleField\"'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.warning','null'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.elements.0.width','100'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.name','\"Content\"'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.fieldLayouts.f22b67dc-4f97-4db4-8b88-9a042cc99733.tabs.0.sortOrder','1'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.handle','\"images\"'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.hasUrls','true'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.name','\"Images\"'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.settings.path','\"@webroot/assets/images\"'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.sortOrder','1'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.titleTranslationKeyFormat','null'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.titleTranslationMethod','null'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.type','\"craft\\\\volumes\\\\Local\"'),
	('volumes.a4e045b6-04b7-4f07-8562-0903dff4098c.url','\"@web/assets/images\"');

/*!40000 ALTER TABLE `craft_projectconfig` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_projectconfignames
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_projectconfignames`;

CREATE TABLE `craft_projectconfignames` (
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_projectconfignames` WRITE;
/*!40000 ALTER TABLE `craft_projectconfignames` DISABLE KEYS */;

INSERT INTO `craft_projectconfignames` (`uid`, `name`)
VALUES
	('2389b641-0d8e-4d1d-a7b6-a255d9ece2cd','Homepage'),
	('2f695e2f-09f2-4d55-8db7-2e1a3e8ddc17','Slider'),
	('40a2e466-827a-47cc-ab67-19eaac358bb0','Slide'),
	('90138bb3-2808-45dd-90b8-553ca7c4d579','Homepage'),
	('9a54084c-6e32-43e1-bb71-0153f402bea7','Heading'),
	('a4e045b6-04b7-4f07-8562-0903dff4098c','Images'),
	('b05f5804-6505-4612-aad0-e5fc8a0d09cb','Common'),
	('b13e99d8-cba3-4b69-81bc-0f2475ef51ff','Image'),
	('b95b3ab1-40f6-44cc-ac72-0a4ceccd2e87','Imagic'),
	('f983df67-3391-43eb-9b66-fca30122395e','Hero image'),
	('fa32c586-4ce5-4e4c-9c24-8a86208c284a','Imagic');

/*!40000 ALTER TABLE `craft_projectconfignames` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_queue
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_queue`;

CREATE TABLE `craft_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'queue',
  `job` longblob NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `timePushed` int(11) NOT NULL,
  `ttr` int(11) NOT NULL,
  `delay` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) unsigned NOT NULL DEFAULT '1024',
  `dateReserved` datetime DEFAULT NULL,
  `timeUpdated` int(11) DEFAULT NULL,
  `progress` smallint(6) NOT NULL DEFAULT '0',
  `progressLabel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempt` int(11) DEFAULT NULL,
  `fail` tinyint(1) DEFAULT '0',
  `dateFailed` datetime DEFAULT NULL,
  `error` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `craft_idx_rvxvueeojvzyhmwavwrfmnlndixqyqlthodn` (`channel`,`fail`,`timeUpdated`,`timePushed`),
  KEY `craft_idx_gpmpjsjmvuvxwbnndtondeoddqhymlgsavcd` (`channel`,`fail`,`timeUpdated`,`delay`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_relations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_relations`;

CREATE TABLE `craft_relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceSiteId` int(11) DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_kquekcurvqhbgbwwtgbtxqsarapbpclezqtc` (`fieldId`,`sourceId`,`sourceSiteId`,`targetId`),
  KEY `craft_idx_jnhcdicqgcxjlddgytjjxznbhsklzakphbwl` (`sourceId`),
  KEY `craft_idx_pdqprwtbgcxqrxdeqkacznoeelbbdwwbyqap` (`targetId`),
  KEY `craft_idx_omubfupajvhrauhnfkmicbssjlmsmaneohvc` (`sourceSiteId`),
  CONSTRAINT `craft_fk_eauxqzwfvanlrldhpuqsgqwmxnonoacblbmx` FOREIGN KEY (`sourceId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_esmutvgypcglwahzeqtccbcvcuspohutptmt` FOREIGN KEY (`sourceSiteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_fk_proppatnjqmmwsgxpgiofxocsbpjviqxqxuu` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_ylbdwhoibyakpkbnbtckplowmexiexnqlwwq` FOREIGN KEY (`targetId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_relations` WRITE;
/*!40000 ALTER TABLE `craft_relations` DISABLE KEYS */;

INSERT INTO `craft_relations` (`id`, `fieldId`, `sourceId`, `sourceSiteId`, `targetId`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,2,8,NULL,5,1,'2021-04-22 01:54:45','2021-04-22 01:54:45','b9110c03-43d2-44e2-a667-65fc53cb7d57'),
	(2,2,9,NULL,6,1,'2021-04-22 01:54:45','2021-04-22 01:54:45','89860402-330d-44df-8b18-9490c508f669'),
	(3,2,10,NULL,7,1,'2021-04-22 01:54:45','2021-04-22 01:54:45','8ab6b7e4-23ec-40e4-af78-01679b4c7902'),
	(4,2,12,NULL,5,1,'2021-04-22 01:54:45','2021-04-22 01:54:45','cbd24161-8b0f-4be9-a951-43edeae8dfb4'),
	(5,2,13,NULL,6,1,'2021-04-22 01:54:45','2021-04-22 01:54:45','3b75198c-e289-4b1c-8e8c-8507ca8820eb'),
	(6,2,14,NULL,7,1,'2021-04-22 01:54:45','2021-04-22 01:54:45','8e5240a0-b286-4423-bde4-4916d5c87cb9'),
	(7,2,16,NULL,5,1,'2021-04-22 02:28:19','2021-04-22 02:28:19','f62eee9d-8a64-4de3-a7e5-136cf95ab76a'),
	(8,2,17,NULL,6,1,'2021-04-22 02:28:19','2021-04-22 02:28:19','4b30b964-4457-48b8-a799-112eb18a6ff1'),
	(9,2,18,NULL,7,1,'2021-04-22 02:28:19','2021-04-22 02:28:19','dda4035f-f0a5-4686-86b7-c5fd57707777'),
	(10,2,20,NULL,5,1,'2021-04-22 02:28:32','2021-04-22 02:28:32','e98b207a-7157-4502-88b5-83180cde81db'),
	(11,2,21,NULL,6,1,'2021-04-22 02:28:32','2021-04-22 02:28:32','fb773098-a0df-491f-91ad-327dd014b47c'),
	(12,2,22,NULL,7,1,'2021-04-22 02:28:32','2021-04-22 02:28:32','d6c418eb-ded5-461c-8e12-f78d8715db5f'),
	(13,4,2,NULL,23,1,'2021-04-22 02:39:16','2021-04-22 02:39:16','1b8da7d6-add0-482b-9b0e-e65c4e6ef01d'),
	(14,4,24,NULL,23,1,'2021-04-22 02:39:16','2021-04-22 02:39:16','feebbd74-e55b-4f6d-86c0-29d810b1cf50'),
	(15,2,25,NULL,5,1,'2021-04-22 02:39:16','2021-04-22 02:39:16','e312aa62-145b-4320-891f-ed0b9cc73dfa'),
	(16,2,26,NULL,6,1,'2021-04-22 02:39:16','2021-04-22 02:39:16','ed18193d-7c93-4a8d-b549-275b956238b7'),
	(17,2,27,NULL,7,1,'2021-04-22 02:39:16','2021-04-22 02:39:16','daedad6a-f40a-4270-8a7e-dfbf18289306');

/*!40000 ALTER TABLE `craft_relations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_resourcepaths
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_resourcepaths`;

CREATE TABLE `craft_resourcepaths` (
  `hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_resourcepaths` WRITE;
/*!40000 ALTER TABLE `craft_resourcepaths` DISABLE KEYS */;

INSERT INTO `craft_resourcepaths` (`hash`, `path`)
VALUES
	('180a54d0','@craft/web/assets/matrixsettings/dist'),
	('1cc7b4cf','@craft/web/assets/edituser/dist'),
	('22bfb7e1','@bower/jquery/dist'),
	('276675bc','@craft/web/assets/editentry/dist'),
	('2f17ef74','@lib/jquery.payment'),
	('3330409c','@lib/velocity'),
	('4196ac33','@craft/web/assets/craftsupport/dist'),
	('46347d06','@app/web/assets/cp/dist'),
	('49901bfe','@craft/web/assets/login/dist'),
	('5aa53805','@lib/vue'),
	('5d0c302d','@app/web/assets/pluginstore/dist'),
	('65fe369a','@craft/web/assets/editsection/dist'),
	('68803bbc','@app/web/assets/fieldsettings/dist'),
	('774e9b70','@lib/axios'),
	('7c0b7bc','@craft/web/assets/dashboard/dist'),
	('7d99db39','@lib/jquery-ui'),
	('7e839177','@craft/web/assets/feed/dist'),
	('825e1d04','@app/web/assets/fields/dist'),
	('864b688f','@lib/fabric'),
	('868ea162','@craft/web/assets/fields/dist'),
	('8fa059b7','@craft/web/assets/admintable/dist'),
	('930dbcb1','@lib/garnishjs'),
	('a0b0ab37','@lib/d3'),
	('a59ff14a','@app/web/assets/editentry/dist'),
	('a9b1b7a4','@lib/jquery-touch-events'),
	('aabdd37e','@app/web/assets/matrix/dist'),
	('ab37bd2d','@lib/prismjs'),
	('ae158b66','@lib/picturefill'),
	('ae6d6f18','@craft/web/assets/matrix/dist'),
	('b6fd8a98','@lib/fileupload'),
	('ba5eb58f','@lib/iframe-resizer'),
	('cdcdb561','@app/web/assets/editsection/dist'),
	('d1435e0b','@craft/web/assets/updater/dist'),
	('d2f1c14','@lib/element-resize-detector'),
	('d8801854','@craft/web/assets/recententries/dist'),
	('dbfcf71a','@app/web/assets/admintable/dist'),
	('e63a5a3a','@craft/web/assets/updateswidget/dist'),
	('eb797963','@craft/web/assets/fieldsettings/dist'),
	('ee562d7','@app/web/assets/matrixsettings/dist'),
	('f007e268','@lib/xregexp'),
	('f138d87d','@craft/web/assets/installer/dist'),
	('f53fb3d6','@craft/web/assets/pluginstore/dist'),
	('fa6ba6e2','@lib/selectize'),
	('fc4483ab','@craft/web/assets/cp/dist');

/*!40000 ALTER TABLE `craft_resourcepaths` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_revisions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_revisions`;

CREATE TABLE `craft_revisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sourceId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `num` int(11) NOT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_chxtmxqydsaseyihohgsaqxgnztruytesfrg` (`sourceId`,`num`),
  KEY `craft_fk_tsqejqfymhivrewnkzhkuzfmlfiwmgdzcwgz` (`creatorId`),
  CONSTRAINT `craft_fk_iyikzrwelhjddmiiyyclzhsyvysuvmspdlcz` FOREIGN KEY (`sourceId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_tsqejqfymhivrewnkzhkuzfmlfiwmgdzcwgz` FOREIGN KEY (`creatorId`) REFERENCES `craft_users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_revisions` WRITE;
/*!40000 ALTER TABLE `craft_revisions` DISABLE KEYS */;

INSERT INTO `craft_revisions` (`id`, `sourceId`, `creatorId`, `num`, `notes`)
VALUES
	(1,2,1,1,NULL),
	(2,2,1,2,NULL),
	(3,2,1,3,''),
	(4,2,1,4,NULL),
	(5,2,1,5,NULL),
	(6,2,1,6,'');

/*!40000 ALTER TABLE `craft_revisions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_searchindex
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_searchindex`;

CREATE TABLE `craft_searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `fieldId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`siteId`),
  FULLTEXT KEY `craft_idx_igharizbchbvztdlvyivijyfhkewuypqrvtt` (`keywords`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_searchindex` WRITE;
/*!40000 ALTER TABLE `craft_searchindex` DISABLE KEYS */;

INSERT INTO `craft_searchindex` (`elementId`, `attribute`, `fieldId`, `siteId`, `keywords`)
VALUES
	(1,'email',0,1,' ladyginamarina gmail com '),
	(1,'firstname',0,1,''),
	(1,'fullname',0,1,''),
	(1,'lastname',0,1,''),
	(1,'slug',0,1,''),
	(1,'username',0,1,' admin '),
	(2,'slug',0,1,' homepage '),
	(2,'title',0,1,' heading '),
	(5,'extension',0,1,' jpeg '),
	(5,'filename',0,1,' photo 1544198365 f5d60b6d8190 jpeg '),
	(5,'kind',0,1,' image '),
	(5,'slug',0,1,''),
	(5,'title',0,1,' photo 1544198365 f5d60b6d8190 '),
	(6,'extension',0,1,' jpeg '),
	(6,'filename',0,1,' photo 1547093349 65cdba98369a jpeg '),
	(6,'kind',0,1,' image '),
	(6,'slug',0,1,''),
	(6,'title',0,1,' photo 1547093349 65cdba98369a '),
	(7,'extension',0,1,' jpeg '),
	(7,'filename',0,1,' photo 1483728642387 6c3bdd6c93e5 jpeg '),
	(7,'kind',0,1,' image '),
	(7,'slug',0,1,''),
	(7,'title',0,1,' photo 1483728642387 6c3bdd6c93e5 '),
	(8,'slug',0,1,''),
	(9,'slug',0,1,''),
	(10,'slug',0,1,''),
	(12,'slug',0,1,''),
	(13,'slug',0,1,''),
	(14,'slug',0,1,''),
	(16,'slug',0,1,''),
	(17,'slug',0,1,''),
	(18,'slug',0,1,''),
	(20,'slug',0,1,''),
	(21,'slug',0,1,''),
	(22,'slug',0,1,''),
	(23,'extension',0,1,' jpeg '),
	(23,'filename',0,1,' photo 1575908524891 b0bdd2b99f90 jpeg '),
	(23,'kind',0,1,' image '),
	(23,'slug',0,1,''),
	(23,'title',0,1,' photo 1575908524891 b0bdd2b99f90 '),
	(25,'slug',0,1,''),
	(26,'slug',0,1,''),
	(27,'slug',0,1,'');

/*!40000 ALTER TABLE `craft_searchindex` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sections`;

CREATE TABLE `craft_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('single','channel','structure') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'channel',
  `enableVersioning` tinyint(1) NOT NULL DEFAULT '0',
  `propagationMethod` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'all',
  `previewTargets` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_wqarnrwprkhpzwbnbvwvgbluzddmqqzhpysw` (`handle`),
  KEY `craft_idx_iuepeildlqrjwhqeiddudtcbvsskseelklyp` (`name`),
  KEY `craft_idx_klckxummcybbugnwwyehoqasjsqnvwhykrae` (`structureId`),
  KEY `craft_idx_zyjeorhsqjdaahxwzaxriuxwqkpkoaqzrjsu` (`dateDeleted`),
  CONSTRAINT `craft_fk_gtstgiprpbbrjaealscvnqqjyylpivytwvvi` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_sections` WRITE;
/*!40000 ALTER TABLE `craft_sections` DISABLE KEYS */;

INSERT INTO `craft_sections` (`id`, `structureId`, `name`, `handle`, `type`, `enableVersioning`, `propagationMethod`, `previewTargets`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`)
VALUES
	(1,NULL,'Homepage','homepage','single',1,'all',NULL,'2021-04-22 01:22:00','2021-04-22 01:22:00',NULL,'90138bb3-2808-45dd-90b8-553ca7c4d579');

/*!40000 ALTER TABLE `craft_sections` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_sections_sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sections_sites`;

CREATE TABLE `craft_sections_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text COLLATE utf8_unicode_ci,
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabledByDefault` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_zxjgmxyshrsvjlfgvyuyrbxwbwtvwprmrlnc` (`sectionId`,`siteId`),
  KEY `craft_idx_phcftyafyajcveprtrapcucshraelriuoqeu` (`siteId`),
  CONSTRAINT `craft_fk_nwxatvypixgszeiyyudvzgjvpkvvtftxfzwe` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_fk_qpxxejmeoqdcfwmqgppfaoczwisuhjnerauy` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_sections_sites` WRITE;
/*!40000 ALTER TABLE `craft_sections_sites` DISABLE KEYS */;

INSERT INTO `craft_sections_sites` (`id`, `sectionId`, `siteId`, `hasUrls`, `uriFormat`, `template`, `enabledByDefault`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,1,1,'__home__','index',1,'2021-04-22 01:22:00','2021-04-22 01:22:00','62effed4-5088-472b-8bec-78e3be5e3d51');

/*!40000 ALTER TABLE `craft_sections_sites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_sequences
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sequences`;

CREATE TABLE `craft_sequences` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `next` int(11) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sessions`;

CREATE TABLE `craft_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `token` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_aiovlydorcdqondnvhnftvbpmgmdcuuclfcy` (`uid`),
  KEY `craft_idx_htrkkddnraukbwwinqmrjaxvptictgmexhyv` (`token`),
  KEY `craft_idx_pylkloyyfvwhyecicjoprjhtutzupxzxemjv` (`dateUpdated`),
  KEY `craft_idx_niqmjpaslpxpngsppzmpfrjxbcsvqxquegqz` (`userId`),
  CONSTRAINT `craft_fk_rcwtaxhlttxdqjjnxveslfvxxlodtzseyrnm` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_sessions` WRITE;
/*!40000 ALTER TABLE `craft_sessions` DISABLE KEYS */;

INSERT INTO `craft_sessions` (`id`, `userId`, `token`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'_ca173COIzNsnseEcm1ozertScvloHngs-slSJ3FBzGVLh_sI1WUGrRbba8aL4Jp2Sn1egwMEEWQSlShGQRZiEtjvt1AyRJgEMrk','2021-04-22 01:21:27','2021-04-22 02:55:10','44ddcad3-2b72-4492-a1eb-9686917be295');

/*!40000 ALTER TABLE `craft_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_shunnedmessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_shunnedmessages`;

CREATE TABLE `craft_shunnedmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_ffxeqwkbcawwehyxauljrbdcvzupyiuzskpx` (`userId`,`message`),
  CONSTRAINT `craft_fk_pbxmcxpeovqqkongtundmghqqvvljyhpjbmm` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_sitegroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sitegroups`;

CREATE TABLE `craft_sitegroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_rbjyoyywmcoqcghsauwfcspxlntakqhwjfpj` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_sitegroups` WRITE;
/*!40000 ALTER TABLE `craft_sitegroups` DISABLE KEYS */;

INSERT INTO `craft_sitegroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`)
VALUES
	(1,'Imagic','2021-04-22 01:07:56','2021-04-22 01:07:56',NULL,'b95b3ab1-40f6-44cc-ac72-0a4ceccd2e87');

/*!40000 ALTER TABLE `craft_sitegroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sites`;

CREATE TABLE `craft_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `primary` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '0',
  `baseUrl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_jdmwumwefykbmbauonhtxdimgifnsjnmkfri` (`dateDeleted`),
  KEY `craft_idx_efykzauzomivsjlbryulqjoqarquullajvku` (`handle`),
  KEY `craft_idx_jouutwmqbntejgwwbmuswedbgfthduvohjxi` (`sortOrder`),
  KEY `craft_fk_ffyadlkswufzubgtrdcszaqyvglfuyciclhx` (`groupId`),
  CONSTRAINT `craft_fk_ffyadlkswufzubgtrdcszaqyvglfuyciclhx` FOREIGN KEY (`groupId`) REFERENCES `craft_sitegroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_sites` WRITE;
/*!40000 ALTER TABLE `craft_sites` DISABLE KEYS */;

INSERT INTO `craft_sites` (`id`, `groupId`, `primary`, `enabled`, `name`, `handle`, `language`, `hasUrls`, `baseUrl`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`)
VALUES
	(1,1,1,1,'Imagic','default','en-US',1,'$PRIMARY_SITE_URL',1,'2021-04-22 01:07:56','2021-04-22 01:07:56',NULL,'fa32c586-4ce5-4e4c-9c24-8a86208c284a');

/*!40000 ALTER TABLE `craft_sites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_structureelements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_structureelements`;

CREATE TABLE `craft_structureelements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_tahvnsujuggywzeknvkkaksbpwvaxuupzlxt` (`structureId`,`elementId`),
  KEY `craft_idx_qgwoaudubttfbjzwsiaaxrrvmbgfkuqrcgxp` (`root`),
  KEY `craft_idx_rghrxyapewtqjzmrccxrbzebauznjnnknydk` (`lft`),
  KEY `craft_idx_okgvipiayjbhpmnvsbhbkubppilrqcukowks` (`rgt`),
  KEY `craft_idx_voqbtefogxrnkuzikcpmskozpeqbuyoivctz` (`level`),
  KEY `craft_idx_aggpxsuswfxnwtyblqswinuigsaumwpcafpj` (`elementId`),
  CONSTRAINT `craft_fk_qnpzjlruntwiqvibasukgdmjqsnyhocsubss` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_tkgqgyvhhwoerilgocnczflmdxoznsvbpjak` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_structures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_structures`;

CREATE TABLE `craft_structures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maxLevels` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_ejeznbofygpczgsgirzhgbkhtqygjawgnvwl` (`dateDeleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_systemmessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_systemmessages`;

CREATE TABLE `craft_systemmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` text COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_lpckhxbprqyikbxqlioyaactlancmvyqirmx` (`key`,`language`),
  KEY `craft_idx_wydfnaamlfozjdaczeaqnsmmjeaxbyacrens` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_taggroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_taggroups`;

CREATE TABLE `craft_taggroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_zwkqllfhfheytshgjccdeaztxoigesjcjyic` (`name`),
  KEY `craft_idx_dgdbdzagnrjtmqhswpnpmjxbmwtaexsmuxzn` (`handle`),
  KEY `craft_idx_rsjrlwyljdynmvitlunogjpadykacivaqiwm` (`dateDeleted`),
  KEY `craft_fk_cortqcdnrgtregpuixmwnclmesouuctrjukh` (`fieldLayoutId`),
  CONSTRAINT `craft_fk_cortqcdnrgtregpuixmwnclmesouuctrjukh` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_tags`;

CREATE TABLE `craft_tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `deletedWithGroup` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_ngxrhhwwutszduycptfmejbqmjognzaacvjq` (`groupId`),
  CONSTRAINT `craft_fk_hrllqdjyuwieegzuxdrtockqcpzwwltnujfw` FOREIGN KEY (`groupId`) REFERENCES `craft_taggroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_nctdydaojpaesywfnchmwvsctjglqktgwwvj` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_templatecacheelements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_templatecacheelements`;

CREATE TABLE `craft_templatecacheelements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `craft_idx_rcowsvyzymkijdddwovsabzepkvminsatfow` (`cacheId`),
  KEY `craft_idx_cklbnhasktqbasbahhennupedjbkcphldeek` (`elementId`),
  CONSTRAINT `craft_fk_hzccponsnhflsmlscnppuuquyamayvnaeqdr` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_nzecctrwulfctugotvkklrybuuaqjcpmqrkq` FOREIGN KEY (`cacheId`) REFERENCES `craft_templatecaches` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_templatecachequeries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_templatecachequeries`;

CREATE TABLE `craft_templatecachequeries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheId` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `query` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `craft_idx_tszgkrihwvnobuvflgmadewdvkkhztsffhrm` (`cacheId`),
  KEY `craft_idx_iauwwqbwcnruzpewpetabuofbpfykryoztbw` (`type`),
  CONSTRAINT `craft_fk_zmfqxscuwklqzjjkmxlcpcguhptaownspuhd` FOREIGN KEY (`cacheId`) REFERENCES `craft_templatecaches` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_templatecaches
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_templatecaches`;

CREATE TABLE `craft_templatecaches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) NOT NULL,
  `cacheKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `craft_idx_gpzvhjngygqcurdksroarticlikwweodykrc` (`cacheKey`,`siteId`,`expiryDate`,`path`),
  KEY `craft_idx_fouewbujhzqszitdyrwjqswnjmxvupfjxvvk` (`cacheKey`,`siteId`,`expiryDate`),
  KEY `craft_idx_hqjjdcgclpojlgepgkoegpypcdewmdlfrdye` (`siteId`),
  CONSTRAINT `craft_fk_fmewibynpsdxmpriqpjkuiaidjgemplddxoz` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_tokens`;

CREATE TABLE `craft_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `route` text COLLATE utf8_unicode_ci,
  `usageLimit` tinyint(3) unsigned DEFAULT NULL,
  `usageCount` tinyint(3) unsigned DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_lubnhqgnlijnhbcguxtivcgbefkglsqxyxkn` (`token`),
  KEY `craft_idx_subblryfcngblqtgiejlcyecaxujeqthrcui` (`expiryDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_usergroups`;

CREATE TABLE `craft_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_roanbopbyfchpylvmwjqjlmofpuqxdwtbqkq` (`handle`),
  KEY `craft_idx_hprpzugsbxzrqkqifdcivwiahcaxotzcnxny` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_usergroups_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_usergroups_users`;

CREATE TABLE `craft_usergroups_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_tfmyrzxjkwxbrnovxtakgywkyrstiruvrmzm` (`groupId`,`userId`),
  KEY `craft_idx_gtotajvvcqpmhmxxfdibystaxiccuspidxsb` (`userId`),
  CONSTRAINT `craft_fk_iuktpyjcffgfdssbkwyvbgsylresrgannfkl` FOREIGN KEY (`groupId`) REFERENCES `craft_usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_kgmdjocobekjmvryhibtillervcuopkhbojq` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_userpermissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_userpermissions`;

CREATE TABLE `craft_userpermissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_tpxkjvlcuprxwewuoaztlgorhkajkipicorm` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_userpermissions_usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_userpermissions_usergroups`;

CREATE TABLE `craft_userpermissions_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_nwrrzeevwkxclkomrojonzfozdgwkcimbsuc` (`permissionId`,`groupId`),
  KEY `craft_idx_nqriuwqliqqnejevwfhzfrlzinqcqpkxrypy` (`groupId`),
  CONSTRAINT `craft_fk_eidklcwxplpjidxbmjttjvgsfoslwsrcmced` FOREIGN KEY (`groupId`) REFERENCES `craft_usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_kwwopvakojrizmlieqzhfubaalzudicoinbl` FOREIGN KEY (`permissionId`) REFERENCES `craft_userpermissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_userpermissions_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_userpermissions_users`;

CREATE TABLE `craft_userpermissions_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_anzyuersumwfbjxetvfwaufjqeivpbovmmxx` (`permissionId`,`userId`),
  KEY `craft_idx_avsapwbetgrzhbtzxnjyntcolqzsvcqyeaha` (`userId`),
  CONSTRAINT `craft_fk_crutmdxkuvwqyvoxqtaxyesyqzsnszrbhnwq` FOREIGN KEY (`permissionId`) REFERENCES `craft_userpermissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_ycclynmnsttxqqweitdzszrwsrswydqsiufr` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_userpreferences
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_userpreferences`;

CREATE TABLE `craft_userpreferences` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `preferences` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`userId`),
  CONSTRAINT `craft_fk_kniliefatbmcxiwzkrovbgstvqbipugegqpg` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_userpreferences` WRITE;
/*!40000 ALTER TABLE `craft_userpreferences` DISABLE KEYS */;

INSERT INTO `craft_userpreferences` (`userId`, `preferences`)
VALUES
	(1,'{\"language\":\"en-US\",\"locale\":null,\"weekStartDay\":\"1\",\"useShapes\":false,\"underlineLinks\":false,\"showFieldHandles\":true,\"enableDebugToolbarForSite\":false,\"enableDebugToolbarForCp\":false,\"showExceptionView\":false,\"profileTemplates\":false}');

/*!40000 ALTER TABLE `craft_userpreferences` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_users`;

CREATE TABLE `craft_users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `photoId` int(11) DEFAULT NULL,
  `firstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `pending` tinyint(1) NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIp` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(3) unsigned DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `hasDashboard` tinyint(1) NOT NULL DEFAULT '0',
  `verificationCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwordResetRequired` tinyint(1) NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_qufkwinrrzvqctlcykykbluhcrxofvhvhmou` (`uid`),
  KEY `craft_idx_vvwyamhpxhnstfmgismcfsktfiuevuebikex` (`verificationCode`),
  KEY `craft_idx_obrsjwudururahbumeqihcaucvkheevrfhay` (`email`),
  KEY `craft_idx_vvrebncgusifvwynmztwxzncfpfrlayrvyaw` (`username`),
  KEY `craft_fk_apeudnbvryfgguvhrpbzzkggynxycpoeujgg` (`photoId`),
  CONSTRAINT `craft_fk_apeudnbvryfgguvhrpbzzkggynxycpoeujgg` FOREIGN KEY (`photoId`) REFERENCES `craft_assets` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_fk_ylournfsikqhzpiebnkdzdifqwmuynwagrlr` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_users` WRITE;
/*!40000 ALTER TABLE `craft_users` DISABLE KEYS */;

INSERT INTO `craft_users` (`id`, `username`, `photoId`, `firstName`, `lastName`, `email`, `password`, `admin`, `locked`, `suspended`, `pending`, `lastLoginDate`, `lastLoginAttemptIp`, `invalidLoginWindowStart`, `invalidLoginCount`, `lastInvalidLoginDate`, `lockoutDate`, `hasDashboard`, `verificationCode`, `verificationCodeIssuedDate`, `unverifiedEmail`, `passwordResetRequired`, `lastPasswordChangeDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'admin',NULL,'','','ladyginamarina@gmail.com','$2y$13$R9YJs9NccleeMhVVStXyi.GVajShoToY4TDmrQ1WwyOKPQqohhqXe',1,0,0,0,'2021-04-22 01:21:27',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,'2021-04-22 01:07:57','2021-04-22 01:07:57','2021-04-22 01:55:43','0957e6d0-fad6-4c0b-a166-6a964a26bd16');

/*!40000 ALTER TABLE `craft_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_volumefolders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_volumefolders`;

CREATE TABLE `craft_volumefolders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_idx_dtlgzzigrsodtasrhugrzlnmfcjbxcseamzl` (`name`,`parentId`,`volumeId`),
  KEY `craft_idx_lwoncleruqrpcoaqbrtyqidqouenfemigpfj` (`parentId`),
  KEY `craft_idx_zzzfvxirawvhuwsxepaybpidkjudegnzlrli` (`volumeId`),
  CONSTRAINT `craft_fk_ovwpyzopcyvvkwkpjlzwoxufrcltkuugdphp` FOREIGN KEY (`volumeId`) REFERENCES `craft_volumes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fk_qqusihvagqodjzywnskibgrwhqafqcsgdpxt` FOREIGN KEY (`parentId`) REFERENCES `craft_volumefolders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_volumefolders` WRITE;
/*!40000 ALTER TABLE `craft_volumefolders` DISABLE KEYS */;

INSERT INTO `craft_volumefolders` (`id`, `parentId`, `volumeId`, `name`, `path`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,1,'Images','','2021-04-22 01:51:12','2021-04-22 01:51:12','712aa06e-c6dc-47bd-8f58-66e797fc01c3'),
	(2,1,1,'hero','hero/','2021-04-22 01:52:52','2021-04-22 01:52:52','b8717971-1bed-47d6-8c5e-5e0a0cb9d736'),
	(3,NULL,NULL,'Temporary source',NULL,'2021-04-22 01:52:52','2021-04-22 01:52:52','f594f0f7-ac3b-4c3e-a228-455f8a42abb7'),
	(4,3,NULL,'user_1','user_1/','2021-04-22 01:52:52','2021-04-22 01:52:52','52856289-d1c1-4a0a-a2aa-ef3afc4c44f2');

/*!40000 ALTER TABLE `craft_volumefolders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_volumes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_volumes`;

CREATE TABLE `craft_volumes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titleTranslationMethod` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'site',
  `titleTranslationKeyFormat` text COLLATE utf8_unicode_ci,
  `settings` text COLLATE utf8_unicode_ci,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_trnoszcjkhantfumcpewtkrlqfadepfephqi` (`name`),
  KEY `craft_idx_tqmlcjxvhhbtsyrizgcupcroxkfldbpajzmz` (`handle`),
  KEY `craft_idx_ksrurnfqbftnefnkbawttlnhmjiaukcgsufr` (`fieldLayoutId`),
  KEY `craft_idx_pfcytrvwmkluxhyeyeenrdrqufjhabrjaohc` (`dateDeleted`),
  CONSTRAINT `craft_fk_sueunhbxxcdtoqlcmcwqiidkdhhgbacwcihg` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_volumes` WRITE;
/*!40000 ALTER TABLE `craft_volumes` DISABLE KEYS */;

INSERT INTO `craft_volumes` (`id`, `fieldLayoutId`, `name`, `handle`, `type`, `hasUrls`, `url`, `titleTranslationMethod`, `titleTranslationKeyFormat`, `settings`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`)
VALUES
	(1,2,'Images','images','craft\\volumes\\Local',1,'@web/assets/images','site',NULL,'{\"path\":\"@webroot/assets/images\"}',1,'2021-04-22 01:51:12','2021-04-22 01:51:12',NULL,'a4e045b6-04b7-4f07-8562-0903dff4098c');

/*!40000 ALTER TABLE `craft_volumes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_widgets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_widgets`;

CREATE TABLE `craft_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `colspan` tinyint(3) DEFAULT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_idx_tvgkflnwjxnhljfgztixgvbkujhmqxiipdmm` (`userId`),
  CONSTRAINT `craft_fk_ccdmuksdlcgjmwraptxeiynzffqneegwxkek` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_widgets` WRITE;
/*!40000 ALTER TABLE `craft_widgets` DISABLE KEYS */;

INSERT INTO `craft_widgets` (`id`, `userId`, `type`, `sortOrder`, `colspan`, `settings`, `enabled`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'craft\\widgets\\RecentEntries',1,NULL,'{\"siteId\":1,\"section\":\"*\",\"limit\":10}',1,'2021-04-22 01:21:28','2021-04-22 01:21:28','aa3cbbca-d78c-46ff-aa7d-b437b09868f4'),
	(2,1,'craft\\widgets\\CraftSupport',2,NULL,'[]',1,'2021-04-22 01:21:28','2021-04-22 01:21:28','7ef9cbff-e786-4281-89b8-efb3c0739c77'),
	(3,1,'craft\\widgets\\Updates',3,NULL,'[]',1,'2021-04-22 01:21:28','2021-04-22 01:21:28','8855c29c-d0a9-486f-917a-143404866f3a'),
	(4,1,'craft\\widgets\\Feed',4,NULL,'{\"url\":\"https://craftcms.com/news.rss\",\"title\":\"Craft News\",\"limit\":5}',1,'2021-04-22 01:21:28','2021-04-22 01:21:28','3303739d-c63b-4b29-b89e-eb000a1a215f');

/*!40000 ALTER TABLE `craft_widgets` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
