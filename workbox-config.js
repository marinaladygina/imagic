module.exports = {
  "globDirectory": "web/",
  "globPatterns": [
    "assets/css/**/*.{css,svg,js,png,woff,jpg,gif,eot,woff2}",
    "assets/js/**/*.{css,js}",
  ],
  "swDest": "web/sw.js",
  "swSrc": "src-sw.js",
};