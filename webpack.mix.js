const mix = require('laravel-mix');

mix.setPublicPath('web')
  .options({ 
    imgLoaderOptions: { enabled: false },
    processCssUrls: false
  })
  .sass('resources/css/vendor.scss', 'web/assets/css')
  .sass('resources/css/app.scss', 'web/assets/css')
  .js('resources/js/vendor.js', 'web/assets/js')
  .version();
   
mix.minify('web/assets/js/vendor.js');
mix.minify('web/assets/css/vendor.css');
mix.minify('web/assets/css/app.css');